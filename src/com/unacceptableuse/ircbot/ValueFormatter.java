package com.unacceptableuse.ircbot;

import java.util.Random;

public class ValueFormatter
{
	
	/**
	 * Assumes all values to be khashes
	 * @param hashrate
	 * @return
	 */
	public static String formatHashrate(long khash)
	{
		if(khash > 1000000000)return khash/1000000000+"th";	//Terahash
		if(khash > 1000000)return khash/1000000+"gh";	//Gigahash
		if(khash > 1000)return khash/1000+"mh";			//Megahash
		if(khash < 1000)return khash+"kh";				//Kilohash
		if(khash < 1)return khash*1000+"h";				//Hash
		return String.valueOf(khash);
	}
	
	 public static String formatTime(long millis)
	 {
		 StringBuilder finishedProduct = new StringBuilder();
		 
		 long second = (millis / 1000) % 60;
		 long minute = (millis / (1000 * 60)) % 60;
		 long hour = (millis / (1000 * 60 * 60)) % 24;
		 long day = (millis / (1000 * 60 * 60 * 24)) % 365;
		 long year = (millis / (1000 * 60 * 60 * 24 * 365));
	 
		 if(day > 1)
			finishedProduct.append(year+" years ");
		 
		 if(day > 1)
			finishedProduct.append(day+" days ");
		 
		if(hour > 1)
			finishedProduct.append(hour+" hours ");
		
		if(minute > 1)
			finishedProduct.append(minute+" minutes ");

		if(second > 1)
			finishedProduct.append(second+" seconds.");
			 
		 return finishedProduct.toString();
	 }
	 
	 public static String chooseFromArray(String[] array)
	 {
		 return array[new Random().nextInt(array.length)];
	 }
}
