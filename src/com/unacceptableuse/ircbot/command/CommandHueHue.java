package com.unacceptableuse.ircbot.command;

public class CommandHueHue extends Command
{

	public CommandHueHue(Object ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
			throws Exception
	{
		sendMessage(channel, "http://huehuehuehue.com");
	}

	@Override
	public String[] getAliases()
	{
		
		return new String[]{"huehue","huehuehuehue"};
	}

	@Override
	public String getHelp()
	{
		
		return "heuheuheuheuheuehueh";
	}
	
	@Override
	public boolean doesAnnoyTnt64()
	{
		return true;
	}

}
