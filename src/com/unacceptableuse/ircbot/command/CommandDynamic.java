package com.unacceptableuse.ircbot.command;

public class CommandDynamic extends Command
{

	
	String function;
	String command;
	public CommandDynamic(Object ub, String command, String function)
	{
		super(ub);
		this.function = function;
		this.command = command;
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
			throws Exception
	{
		String[] args = message.split(" ");
		StringBuilder parsedMessage = new StringBuilder();
		
		
//		if(function.contains(" "))
//		{
			
			for(String s : function.split(" "))
			{
				if(s.startsWith("rand:"))
					parsedMessage.append(ub.rand.nextInt(Integer.parseInt(s.split(":")[1])));
				else
				if(s.startsWith("args:"))
					parsedMessage.append(args.length < Integer.parseInt(s.split(":")[1])+1 ? "Arg Required" : args[Integer.parseInt(s.split(":")[1])]);
				else
					if(s.equalsIgnoreCase("&sender"))parsedMessage.append(sender);
				else
					if(s.equalsIgnoreCase("&channel"))parsedMessage.append(channel);
				else
					if(s.equalsIgnoreCase("&message"))parsedMessage.append(message);
				else
					if(s.equalsIgnoreCase("&bot_name"))parsedMessage.append(ub.BOT_NAME);
				else
					if(s.equalsIgnoreCase("&accesslevel"))parsedMessage.append(ub.getAccessLevel(sender));
				else
					if(s.equalsIgnoreCase("&catfact"))parsedMessage.append(ub.generateRandomCatFact());
				else
					
					if(s.startsWith("choose:"))
					{
						String[] ar = s.split(":");
						parsedMessage.append(ar[ub.rand.nextInt(ar.length-1)+1]);
					}
				else
				parsedMessage.append(s);
				
				parsedMessage.append(' ');
			}
			sendMessage(channel, parsedMessage.toString().replace("_"," ").replace("NOSPACE ",""));
//		}
//		else
//		{
//			sendMessage(channel, function);
//		}
			

		
	}

	@Override
	public String[] getAliases()
	{
		
		return new String[]{command, "alldyanmiccommands"};
	}

	@Override
	public String getHelp()
	{
		
		return "Dynamic command";
	} 

}
