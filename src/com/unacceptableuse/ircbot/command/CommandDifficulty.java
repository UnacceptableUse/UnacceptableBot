package com.unacceptableuse.ircbot.command;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.unacceptableuse.ircbot.UnacceptableBot;

public class CommandDifficulty extends Command
{

	public CommandDifficulty(UnacceptableBot ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message) throws Exception
	{
		//Document doc = Jsoup.connect("http://shibepool.com/index.php?page=statistics&action=pool").get();
		//sendMessage(channel, "Current Hashrate:"+doc.getElementsByClass("module_content").get(0).getAllElements().get(0));
		sendMessage(channel, "Somewhat difficult probably");
	}

	@Override
	public String[] getAliases()
	{		
		return new String[]{"difficulty"};
	}

	@Override
	public String getHelp()
	{	
		return " - Difficulty of shibepool";
	}

}
