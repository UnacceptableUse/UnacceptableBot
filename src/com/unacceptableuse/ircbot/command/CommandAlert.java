package com.unacceptableuse.ircbot.command;

import com.unacceptableuse.ircbot.UnacceptableBot;

public class CommandAlert extends Command
{

	public CommandAlert(UnacceptableBot ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
	{
		String[] args = message.split(" ");
		StringBuilder msg = new StringBuilder();
		String[] usernames = ub.findOnlineUsernames(args[1].toLowerCase(), channel);
		if(usernames == null)
		{
			sendMessage(channel, "Could not find person you're talking about.");
		}else
		{
			try{
			for(String s : usernames)
			{
				msg.append(s + " ");
			}
			if(args[1].equals("joel"))
			{
				ub.pushoverAlert("uwfYHearPaobMn5kMoY43Ba5VJQ37Z", sender, channel);
			}
			else if(args[1].equals("neil"))
			{
				ub.pushoverAlert("uK2gCNqiECycAg8G4oJKnBLYXwwEuD", sender, channel);
			}
			else if(args[1].equals("peter"))
			{
				ub.pushoverAlert("ugRUVgjc9X7A6boZf6my4cPPJu1Qje", sender, channel);
			}
			else if(args[1].equals("jake"))
			{
				sendMessage(channel, "Seriously Jake get a pushover key ASAP");
			}

			sendMessage(channel, msg.toString());
			}catch(Exception e)
			{
				sendMessage(channel, "An error occurred: "+e.getMessage());
				e.printStackTrace();
			}
//			pushAlert(sender);
		}
	}

	@Override
	public String[] getAliases()
	{		
		return new String[]{"alert"};
	}

	@Override
	public String getHelp()
	{		
		return " <name> - Pings all usernames belonging to that person, or sends them a pushover alert";
	}

}
