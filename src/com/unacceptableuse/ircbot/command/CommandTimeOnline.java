package com.unacceptableuse.ircbot.command;

import com.unacceptableuse.ircbot.UnacceptableBot;
import com.unacceptableuse.ircbot.ValueFormatter;

public class CommandTimeOnline extends Command
{

	public CommandTimeOnline(Object ub)
	{
		super(ub);
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
			throws Exception
	{
		String[] args = message.split(" ");
		
		if(args.length < 2)
			sendMessage(channel, "You have been online for a total of "+(ub.timeOnline.containsKey(sender) ? ValueFormatter.formatTime(ub.timeOnline.get(sender)) : ValueFormatter.formatTime(UnacceptableBot.getTime()-ub.returnTimes.get(sender))));
		else
			sendMessage(channel, args[1]+" have been online for a total of "+(ub.timeOnline.containsKey(args[1]) ? ValueFormatter.formatTime(ub.timeOnline.get(args[1])) : ValueFormatter.formatTime(UnacceptableBot.getTime()-ub.returnTimes.get(sender))));
	}

	@Override
	public String[] getAliases()
	{
		
		return new String[]{"timeonline","online"};
	}

	@Override
	public String getHelp()
	{
		
		return "[user] - Shows you how long you've been online";
	}
	
}
