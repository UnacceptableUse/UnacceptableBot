package com.unacceptableuse.ircbot.command;

import java.io.InputStreamReader;

import org.jsoup.Jsoup;


import org.jsoup.nodes.Document;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.unacceptableuse.ircbot.UnacceptableBot;

public class CommandMiners extends Command
{

	public CommandMiners(UnacceptableBot ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message) throws Exception
	{
		String[] args = message.toLowerCase().split(" ");
		
		if(args.length < 2)
		{
			sendSyntax(channel);
			return;
		}
		
		JsonParser json = new JsonParser();
		JsonObject currentMinersObject = json.parse(new InputStreamReader(ub.getHTTPSUrlContents("https://coinking.io/api.php?key=c639476cfa3a8ab1bdf8a1532d7c5ddb&type=minersbycoin&coin=" + args[1] + "&output=json"))).getAsJsonObject();

		sendMessage(channel, String.format("Current Active Miners: %s", currentMinersObject.get("workers").getAsString()));
	}

	@Override
	public String[] getAliases()
	{
		return new String[]{"mining", "miners"};
	}

	@Override
	public String getHelp()
	{
		
		return "<cointicker> - Shows current active miners";
	}

}
