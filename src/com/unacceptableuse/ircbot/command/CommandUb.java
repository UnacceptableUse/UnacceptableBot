package com.unacceptableuse.ircbot.command;

import com.unacceptableuse.ircbot.UBStatic;
import com.unacceptableuse.ircbot.UnacceptableBot;

public class CommandUb extends Command
{

	
	
	public CommandUb(UnacceptableBot ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
	{
		String[] args = message.split(" ");
		if(args.length < 2)
			sendSyntax(channel);
		else
		{
			if(args[1].equals("help"))
				sendMessage(channel, "Help can be found here: http://"+UBStatic.SITE_ROOT+"/help.html");
			else if(args[1].equals("version"))
				sendMessage(channel, ub.BOT_NAME+" "+UnacceptableBot.version);
			else if(args[1].equals("source"))
			{
				if(args.length > 2)
				{
					if(args[2].startsWith("Command"))
					{
						sendMessage(channel, "com.unacceptableuse.ircbot.command."+args[2]+": "+UBStatic.SOURCE_ROOT+"/blob/master/src/com/unacceptableuse/ircbot/command/"+args[2]+".java");
					}else
					{
						sendMessage(channel, "com.unacceptableuse.ircbot."+args[2]+": "+UBStatic.SOURCE_ROOT+"/blob/master/src/com/unacceptableuse/ircbot/"+args[2]+".java");
					}
				}else
				{
					sendMessage(channel, "You can find my source here: "+UBStatic.SOURCE_ROOT);
				}
				
			}
				
			else if(args[1].equals("credits"))
				sendMessage(channel, ub.BOT_NAME+" uses &BOLDPircBotX&RESET. Made by &RED&BOLDUnacceptableUse&RESET, &BOLDdvd604&RESET and &BOLDteknogeek&RESET.");
				sendMessage(channel, "Special thanks to @matrixdevuk // matrixdevuk over at irc.matrixdevuk.pw for hosting the bot's website.");
		}
	}

	@Override
	public String[] getAliases()
	{
		
		return new String[]{"ub", "unacceptablebot", "help"};
	}

	@Override
	public String getHelp()
	{
		
		return " help/version/source/credits - Various information about the bot";
	}

}
