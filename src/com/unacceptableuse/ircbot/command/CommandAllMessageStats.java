package com.unacceptableuse.ircbot.command;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.HashMap;

import org.jibble.pircbot.User;

import com.unacceptableuse.ircbot.UnacceptableBot;

public class CommandAllMessageStats extends Command
{

	public CommandAllMessageStats(UnacceptableBot ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
	{
		final String[] args = message.split(" ");
		final String target = args.length > 1 ? args[1] : sender;
		
		try{
			
			int totalCount = 0;//, totalLines = 0;
			HashMap<String, Integer[]> count = new HashMap<String, Integer[]>();
			File[] files = new File("./").listFiles();
			ArrayList<File>logs = new ArrayList<File>();
			long oldestFileTime = System.currentTimeMillis();
			for(File f : files)
			{
				if(f.isDirectory() || !f.canRead())continue;
				if(f.getName().startsWith("#") && !f.getName().contains("copy"))
				{
						logs.add(f);
				}
				
			}
			
			sendMessage(channel, "Processing "+logs.size()+" logs... This might take a while");
			for(File log : logs)
			{
				int icount = 0;
				int totalLines = 0;
				BufferedReader br = new BufferedReader(new FileReader(channel+".log"));
				String line = br.readLine();
				while((line = br.readLine()) != null)
				{
					if(line.toLowerCase().contains(target.toLowerCase()))
						icount++;
					totalLines++;
				}
				count.put(log.getName(), new Integer[]{icount, totalLines});
				
				
			}
			
			for(String s : count.keySet())
			{
				totalCount+= count.get(s)[0];
				//totalLines+= count.get(s)[1];
			}
			
		
	
			
			
			
			sendMessage(channel,"&BOLD"+target+"&RESET has sent &BOLD"+count.get(channel+".log")[0]+"&RESET messages in &BOLD"+channel+"&RESET. &BOLD"+(count.get(channel+".log")[0]*100/count.get(channel+".log")[1])+"&RESET% of all messages.");
			sendMessage(channel, "&BOLD"+target+"&RESET has sent &BOLD"+totalCount+"&RESET messages over &BOLD"+count.size()+"&RESET channels. &BOLDFuck knows I CBA fixing this&RESET% of all messages.");
			
			
			
		}catch(Exception e)
		{
			sendMessage(channel, "&REDSHIT!&RESET: "+e.getMessage()+" "+e.getStackTrace()[0]);
		}
		
	}

	@Override
	public String[] getAliases()
	{
		return new String[]{"allmessagestats"};
	}

	@Override
	public String getHelp()
	{
		return " - Shows detailed message statistics";
	}
	
	@Override
	public boolean doesAnnoyTnt64()
	{
		return true;
	}

}
