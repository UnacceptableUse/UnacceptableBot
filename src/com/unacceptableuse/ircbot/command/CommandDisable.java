package com.unacceptableuse.ircbot.command;

import com.unacceptableuse.ircbot.UnacceptableBot;

public class CommandDisable extends Command
{

	public CommandDisable(UnacceptableBot ub)
	{
		super(ub);
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
	{
		String[] args = message.split(" ");
		
		if(args.length < 0 )
		{
			sendSyntax(channel);
			return;
		}
		
		if(args[1].equalsIgnoreCase("enable"))
		{
			sendMessage(channel, "&REDYou can't disable enable! That's silly!");
		}
		
		ub.disabledCommands.add(args[1]);
		sendMessage(channel, "Disabled command "+args[1]);
	}

	@Override
	public String[] getAliases()
	{
		return new String[]{"disable"};
	}

	@Override
	public String getHelp()
	{
		return " <command> - Disables a command or setting";
	}
	
	@Override
	public int getAccessLevel()
	{
		return 4;
	}

}
