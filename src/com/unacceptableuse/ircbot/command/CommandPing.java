package com.unacceptableuse.ircbot.command;

import com.unacceptableuse.ircbot.UnacceptableBot;

public class CommandPing extends Command
{

	public CommandPing(UnacceptableBot ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
	{
		String args[] = message.split(" ");
		if(args.length < 2)
			sendSyntax(channel);
		else
		{
			sendMessage(channel, "Pinging "+args[1]+"...");
			int ping = ub.pingUrl(args[1]);
			sendMessage(channel, ping == -1 ? "Unable to connect to "+args[1] : ping > 100000 ? "HTTP Error "+ping/1000 : args[1]+" responded in "+ping+"ms");
		}
	}

	@Override
	public String[] getAliases()
	{
		return new String[]{"ping"};
	}

	@Override
	public String getHelp()
	{
		return " <url> - Pings a URL";
	}

}
