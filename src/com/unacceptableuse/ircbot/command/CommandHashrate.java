package com.unacceptableuse.ircbot.command;

import java.io.InputStreamReader;
import java.util.Map.Entry;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.unacceptableuse.ircbot.UnacceptableBot;
import com.unacceptableuse.ircbot.ValueFormatter;

public class CommandHashrate extends Command
{

	public CommandHashrate(UnacceptableBot ub)
	{
		super(ub);
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message) throws Exception
	{
		String[] args = message.toLowerCase().split(" ");
		
		
		if(args.length < 2){sendSyntax(channel);return;}
		if(args.length >= 3)
			if(!args[2].equals("mhash") && !args[2].equals("khash") && !args[2].equals("ghash")){sendSyntax(channel);return;}
		
				
		
		JsonParser json = new JsonParser();
		JsonObject coinObject = json.parse(new InputStreamReader(ub.getHTTPSUrlContents("https://coinking.io/api.php?key=c639476cfa3a8ab1bdf8a1532d7c5ddb&type=poolhashratebycoin&coin="+args[1].toLowerCase()+"&output=json"))).getAsJsonObject();
	
		sendMessage(channel, coinObject.get("mhash").getAsLong() == 0 ? args[1]+" is not being mined." :String.format("%s is being mined at %s%s/s",args[1], args.length < 3 ? coinObject.get("mhash").getAsLong()+"mh" : coinObject.get(args[2]), args.length < 3 ? "" : args[2].substring(0,2)));	

	}

	@Override
	public String[] getAliases()
	{
		
		return new String[]{"hash", "hashes", "hashrate"};
	}

	@Override
	public String getHelp()
	{
		
		return "<coin> khash/mhash/ghash - Displays the current pool hashrate for the coin (Defaults to mhash)";
	}

}
