package com.unacceptableuse.ircbot.command;

public class CommandRemoveCommand extends Command
{

	public CommandRemoveCommand(Object ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
			throws Exception
	{
		String[] args = message.split(" ");
		if(args.length < 2){sendSyntax(channel);return;}
		for(Command c : ub.commands)
		{
			for(String s : c.getAliases())
			{
				if(s.equalsIgnoreCase(args[1]))
				{
					ub.commands.remove(c);
					sendMessage(channel, "Successfully removed command");
					return;
				}
					
			}
		}
		sendMessage(channel, "Unable to find that command!");
	}

	@Override
	public String[] getAliases()
	{
		
		return new String[]{"rm"};
	}

	@Override
	public String getHelp()
	{
		
		return "<command> - Removes a command";
	}

}
