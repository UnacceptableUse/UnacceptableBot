package com.unacceptableuse.ircbot.command;

import com.unacceptableuse.ircbot.UnacceptableBot;

public class CommandRand extends Command
{

	public CommandRand(UnacceptableBot ub)
	{
		super(ub);
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
	{
		String[] choice = message.replace("!rand","").replace("!choice","").split(",");
		if(choice.length < 2)
			sendSyntax(channel);
		else
		{
			String chosen = choice[ub.rand.nextInt(choice.length)];
			sendMessage(channel, chosen.startsWith("!") ? "&REDYou can't use !rand to perform commands!" : chosen);
		}
			
	}

	@Override
	public String[] getAliases()
	{
		return new String[]{"rand", "choice"};
	}

	@Override
	public String getHelp()
	{
		return " <choice1>,<choice2>,<choice3> - Chooses a random choice from your selection";
	}

}
