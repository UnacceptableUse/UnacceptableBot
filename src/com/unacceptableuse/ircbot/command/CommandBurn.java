package com.unacceptableuse.ircbot.command;

import com.unacceptableuse.ircbot.ValueFormatter;

public class CommandBurn extends Command 
{

	public CommandBurn(Object ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
			throws Exception
	{
		
		final String[] names = {"Jimmy","Jamal","Hamlar","Johnny","Pringers","Ogla","Gary","Harry","Dylan"};
		final String[] famousPeople = {"Richard Branson","Jimmy Carr","Jesus","God","slaves","slaves"};
		final String[] burnType = {"rekt","burn","owned"};
		final String[][] burns = {
				{"much celebrities are raising funds to help rebuild.", "hard you now have e-rekt-ile dysfunction"},
				{"hot several pensioners died from the heatwave","hot heatwaves across the UK caused casualties", "hot "+ValueFormatter.chooseFromArray(names)+" has to walk an extra "+ub.rand.nextInt(50)+" miles for water"},
				{ "much "+ValueFormatter.chooseFromArray(famousPeople)+" would be jealous"}
				};
		
		int choice = ub.rand.nextInt(burnType.length);
		sendMessage(channel, String.format("%s so %s.", burnType[choice], ValueFormatter.chooseFromArray(burns[choice])));
	}

	@Override
	public String[] getAliases()
	{
		
		return new String[]{"burn"};
	}

	@Override
	public String getHelp()
	{
		
		return "creates a suitable catchphrase for your epic burn" ;
	}

}
