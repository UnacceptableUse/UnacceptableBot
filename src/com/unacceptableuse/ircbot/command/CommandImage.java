package com.unacceptableuse.ircbot.command;

import java.io.InputStream;
import java.io.InputStreamReader;

import com.unacceptableuse.ircbot.UnacceptableBot;

public class CommandImage extends Command
{

	public CommandImage(UnacceptableBot ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
	{
		String[] args = message.split(" ");
		if(args.length < 2)
			sendSyntax(channel);
		else
			sendMessage(channel, getRandomImage(args[1]));
	}
	
	private String getRandomImage(String subreddit)
	{
		
		if(ub.disabledCommands.contains("image:"+subreddit))return "This subreddit has been disabled";
		try{

		
			InputStream is = ub.getUrlContents("http://api.reddit.com/r/"+subreddit.replace(",","").replace(".",""));
			com.google.gson.JsonParser parser = new com.google.gson.JsonParser(); 

			String imageURL = "Error";
			int timeout = 0;
			boolean isNSFW = false;
			while(!imageURL.contains("imgur") && timeout < 40)
			{
				com.google.gson.JsonObject object = 
						parser.parse(new InputStreamReader(is)).getAsJsonObject()
						.get("data").getAsJsonObject()
						.get("children").getAsJsonArray()
						.get(ub.rand.nextInt(20)).getAsJsonObject()
						.get("data").getAsJsonObject();
				isNSFW = object.get("over_18").getAsBoolean();
				imageURL = object.get("title").getAsString()+": "+object.get("url").getAsString();
				timeout++;
			}
			
			is.close();
			

		return imageURL.contains("imgur") ?  imageURL + (isNSFW ? "&RED&BOLD[NSFW]&RESET" : ""): "Could not find image in that sub!" ;
		}catch(IllegalStateException e)
		{
			sendMessage(UnacceptableBot.BOT_DEBUG_CHANNEL, "IlligalStateException "+e+" "+e.getMessage());
			return getRandomImage(subreddit);
		}
		catch(IndexOutOfBoundsException e)
		{
			return "That subreddit does not exist or does not contain enough images.";
		}
		catch(Exception e)
		{
			e.printStackTrace();
			UnacceptableBot.logException("[UNHANDLED EXCEPTION] Getting random image from "+subreddit, e);
			return "You crashed the bot, so have a gif of two kittens crashing into eachother: http://stream1.gifsoup.com/view6/2612041/kitty-crash-test-no-3-o.gif  %RED"+e.getMessage();
		}
	}

	@Override
	public String[] getAliases()
	{
		return new String[]{"image"};
	}

	@Override
	public String getHelp()
	{
		return " <subreddit> - Gets a random image from the subreddit";
	}

}
