package com.unacceptableuse.ircbot.command;

import java.io.BufferedReader;
import java.io.FileReader;

import com.unacceptableuse.ircbot.UnacceptableBot;

public class CommandMessageStats extends Command
{

	public CommandMessageStats(UnacceptableBot ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
	{
		String[] args = message.split(" ");
		final String target = args.length > 1 ? args[1] : sender;
		boolean countAliases = message.contains("allnicks");
		int count = 0;
		try{
			BufferedReader br = new BufferedReader(new FileReader(channel+".log"));
			String line = br.readLine();
			while((line = br.readLine()) != null)
				if(countAliases){
					for(String s : ub.pastNicks.get(ub.getUserFromNickname(target)))
						if(line.contains("<"+s+">"))
							count++;			
				}else
					if(line.toLowerCase().contains("<"+target.toLowerCase()+">"))
						count++;
				
			
			br.close();
			sendMessage(channel, target+" has sent "+count+" messages.");
			
		}catch(Exception e)
		{
			sendMessage(channel, "Exception... somehow... "+e.getMessage());
		}
	}

	@Override
	public String[] getAliases()
	{
		return new String[]{"messagestats"};
	}

	@Override
	public String getHelp()
	{
		return "[username] - shows stats about an individual, or general stats.";
	}

}
