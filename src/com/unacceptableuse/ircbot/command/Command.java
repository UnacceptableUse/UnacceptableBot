package com.unacceptableuse.ircbot.command;

import org.jibble.pircbot.Colors;

import com.unacceptableuse.ircbot.UnacceptableBot;

public abstract class Command
{

	
	protected UnacceptableBot ub;
	
	public Command(Object ub)
	{
		this.ub = (UnacceptableBot) ub;
	}
	
	
	public abstract void excecuteCommand(String channel, String sender, String message) throws Exception;
	
	public int getAccessLevel()
	{
		return 0;
	}
	
	public abstract String[] getAliases();
	
	public abstract String getHelp();
	
	
	public void sendMessage(String channel, String message)
	{
		
//		if(message.startsWith(".") | message.startsWith("!"))
//		{
//			message = ";) "+message;
//		}
		
		if(!ub.disabledCommands.contains("verbose"))
		{
			ub.sendMessage(UnacceptableBot.BOT_DEBUG_CHANNEL, "("+ub.BOT_NAME+" -> "+channel+") "+message);
		}
		
		if(ub.disabledCommands.contains("pridemode"))
		{
			if(ub.disabledCommands.contains("formattingcodes"))
			{
				ub.sendMessage(channel, message);
			}else
			{
				ub.sendMessage(channel, message
						.replace("&BOLD", Colors.BOLD)
						.replace("&UNDERLINE", Colors.UNDERLINE)
						.replace("&REVERSE", Colors.REVERSE)
						.replace("&PURPLE", Colors.PURPLE)
						.replace("&RED", Colors.RED)
						.replace("&GREEN", Colors.GREEN)
						.replace("&BLUE", Colors.BLUE)
						.replace("&YELLOW", Colors.YELLOW)
						.replace("&BLACK", Colors.BLACK)
						.replace("&CYAN", Colors.CYAN)
						.replace("&WHITE", Colors.WHITE)				
						.replace("&RESET", Colors.NORMAL)
						.replace("&CATFACT", ub.generateRandomCatFact()));
			}
			
		}else
		{
			StringBuilder stb = new StringBuilder();
			for(String s : message.split(" "))
			{
				int rand = ub.rand.nextInt(14);
				switch(rand)
				{
					case 0: stb.append(Colors.BOLD); break;
					case 1: stb.append(Colors.UNDERLINE); break;	
					case 2: stb.append(Colors.REVERSE); break;
					case 3: stb.append(Colors.PURPLE); break;
					case 4: stb.append(Colors.RED); break;
					case 5: stb.append(Colors.GREEN); break;
					case 6: stb.append(Colors.BLUE); break;
					case 7: stb.append(Colors.YELLOW); break;
					case 8: stb.append(Colors.BLACK); break;
					case 9: stb.append(Colors.CYAN); break;
					case 10: stb.append(Colors.WHITE); break;
					case 11: stb.append(Colors.NORMAL); break;
					case 12: stb.append(Colors.TEAL); break;
					case 13: stb.append(Colors.LIGHT_GRAY); break;
					case 14: stb.append(Colors.DARK_GREEN); break;
				
				}
				stb.append(" "+s);
			}
			
			ub.sendMessage(channel, stb.toString()
					.replace("&BOLD", Colors.BOLD)
					.replace("&UNDERLINE", Colors.UNDERLINE)
					.replace("&REVERSE", Colors.REVERSE)
					.replace("&PURPLE", Colors.PURPLE)
					.replace("&RED", Colors.RED)
					.replace("&GREEN", Colors.GREEN)
					.replace("&BLUE", Colors.BLUE)
					.replace("&YELLOW", Colors.YELLOW)
					.replace("&BLACK", Colors.BLACK)
					.replace("&CYAN", Colors.CYAN)
					.replace("&WHITE", Colors.WHITE)				
					.replace("&RESET", Colors.NORMAL));
		}

	}
	
	public void sendSyntax(String channel)
	{
		sendMessage(channel, "!"+getAliases()[0]+" "+getHelp());
	}
	
	
	public boolean doesAnnoyTnt64()
	{
		return false;
	}

}
