// $codepro.audit.disable commandExecution
package com.unacceptableuse.ircbot.command;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class CommandQuote extends Command
{

	public CommandQuote(Object ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)throws Exception
	{
		ArrayList<String> arr = new ArrayList<String>();
		String[] args = message.split(" ");
		
		
		if(args.length < 2)
		{
			sendSyntax(channel);
			return;
		}
		
		Process p = Runtime.getRuntime().exec("grep <" + args[1] + "> " + (args.length == 3 ? (args[2].startsWith("#") ? args[2] : "#" + args[2]) : channel) + ".log");
		
		BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
		
		String line ="";
		while((line = br.readLine()) != null)
		{
			arr.add(line);
		}
		
		if(arr.size() == 0){sendMessage(channel, "Unable to find a quote from that username. (Remember usernames are case sensitive!)");return;}
		
		sendMessage(channel, "'" + arr.get(ub.rand.nextInt(arr.size())).split("> ")[1] + "' - " + args[1]);
		
		
	}

	@Override
	public String[] getAliases()
	{
		
		return new String[]{"quote"};
	}

	@Override
	public String getHelp()
	{
		
		return "<user> [channel] - Generates a random quote from the user supplied";
	}

}
