package com.unacceptableuse.ircbot.command;

public class CommandDefaultLocation extends Command
{

	public CommandDefaultLocation(Object ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
			throws Exception
	{
		String[] args = message.split(" ");
		
		if(args.length < 1){sendSyntax(channel);return;}
		
		
		ub.defaultStatus.put(args[1], message.replace(args[0]+" "+args[1]+" ", ""));
	}

	@Override
	public String[] getAliases()
	{
		
		return new String[]{"defaultstatus","defstat","defaultlocation","defloc"};
	}

	@Override
	public String getHelp()
	{
		
		return "changes your default location";
	}

}
