package com.unacceptableuse.ircbot.command;

import java.io.UnsupportedEncodingException;

import com.unacceptableuse.ircbot.UnacceptableBot;

public class CommandShutdown extends Command 
{

	public CommandShutdown(UnacceptableBot ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
	{
		if(!ub.aloudToMess){sendMessage(channel, "This is why we can't have nice things");return;}
		sendMessage(channel, "Bot shutting down");
		try
		{
			ub.saveThings();
		} catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}
		ub.partChannel(channel, "Bot shutting down");
		ub.disconnect();
		System.exit(0);
	}

	@Override
	public String[] getAliases()
	{
		return new String[]{"shutdown"};
	}

	@Override
	public String getHelp()
	{
		return " - Shuts down the bot";
	}
	
	@Override
	public int getAccessLevel()
	{
		return 2;
	}

}
