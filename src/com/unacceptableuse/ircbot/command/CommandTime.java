package com.unacceptableuse.ircbot.command;

import com.unacceptableuse.ircbot.UnacceptableBot;

public class CommandTime extends Command
{

	public CommandTime(UnacceptableBot ub)
	{
		super(ub);
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
	{
          sendMessage(channel, "The time is now " + new java.util.Date().toString());
	}

	@Override
	public String[] getAliases()
	{
		return new String[]{"time","thetime"};
	}

	@Override
	public String getHelp()
	{	
		return " - Displays the current local server time";
	}

}
