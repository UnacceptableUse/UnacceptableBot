package com.unacceptableuse.ircbot.command;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class CommandDefineUD extends Command
{
	public String multiString;
	public InputStream is;
	
	public CommandDefineUD(Object ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message) throws Exception
	{
		String[] args = message.toLowerCase().split(" ");
		
		if(args.length < 2)
		{
			sendSyntax(channel);
			return;
		}
		
		StringBuilder sb = new StringBuilder();
		
		if(args.length > 2)
		{
			for(int i = 1; i < args.length; i++)
			{
				if(sb.toString().equals(""))
				{
					sb.append(args[i]);
				}
				else
				{
					sb.append("+" + args[i]);
				}
			}
			multiString = sb.toString();
		}
		else
		{
			multiString = args[1];
		}
		
		try {
			
			URL url = new URL("http://api.urbandictionary.com/v0/define?term=" + multiString);
			
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			is =  conn.getInputStream();

		} catch (Exception e2) {
			e2.printStackTrace();
		}
		
		JsonParser json = new JsonParser();
		JsonObject udObject = json.parse(new InputStreamReader(is)).getAsJsonObject();
		try
		{
			sendMessage(channel,
						String.format("%s: %s",
						multiString.replace("+", " "),
						udObject.get("list").getAsJsonArray().get(0).getAsJsonObject().get("definition").toString().replace("\"","")));
		} catch(IndexOutOfBoundsException e3)
		{
			sendMessage(channel, "Word not found!");
		}
	}

	@Override
	public String[] getAliases()
	{
		return new String[]{"defineud"};
	}

	@Override
	public String getHelp()
	{
		return "<word> - Defines current word with Urban Dictionary";
	}
	
	@Override
	public boolean doesAnnoyTnt64()
	{
		return true;
	}

}
