package com.unacceptableuse.ircbot.command;

import java.io.UnsupportedEncodingException;

import com.unacceptableuse.ircbot.UnacceptableBot;

public class CommandBotBeGone extends Command
{

	public CommandBotBeGone(UnacceptableBot ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
	{
		if(!ub.aloudToMess){sendMessage(channel, "This is why we can't have nice things");return;}
		try
		{
			ub.saveThings();
		} catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}
		ub.partChannel(channel, sender+" wants "+ub.BOT_NAME+" to leave, bye bye :(");
	}

	@Override
	public String[] getAliases()
	{
		return new String[]{"botbegone", "leave"};
	}

	@Override
	public String getHelp()
	{
		return " - Tells the bot to leave";
	}

}
