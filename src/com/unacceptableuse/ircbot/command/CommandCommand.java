// $codepro.audit.disable commandExecution
package com.unacceptableuse.ircbot.command;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CommandCommand extends Command
{

	public CommandCommand(Object ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message) throws Exception
	{
		String[] args = message.split(" ");
		
		if(message.contains("rm"))
		{
			sendMessage(channel, "&REDNice try, you are now banned from using "+ub.BOT_NAME+" ever.");
			ub.accessLevels.put(sender, -1);
			return;
		}
		
		if(ub.disabledCommands.contains("console:"+args[1]))
		{
			sendMessage(channel, "That command has been disabled.");
			return;
		}
		try{
		Process p = Runtime.getRuntime().exec(message.split(args[0]+" ")[1]);
		BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
		
		String line ="";
		int lineCount = 0;
		while((line = br.readLine()) != null)
		{
			lineCount++;
			sendMessage(channel, line);
			if(ub.disabledCommands.contains("consoleOutput"))return;
			if(ub.disabledCommands.contains("consoleDisableAt:"+lineCount))
			{
				sendMessage(channel, "&REDLine exceeds maximum line count.");
				break;
			}
		}
		
		br.close();
		}catch(IOException e)
		{
			sendMessage(channel, "The command "+args[0]+" was not recognized.");
		}
		
		
		
	}

	@Override
	public String[] getAliases()
	{		
		return new String[]{"command","cmd"};
	}

	@Override
	public String getHelp()
	{	
		return "<cmd> - Performs a console command";
	}
	
	@Override
	public int getAccessLevel()
	{
		return 4;
	}
	
	@Override
	public boolean doesAnnoyTnt64()
	{
		return true;
	}

}
