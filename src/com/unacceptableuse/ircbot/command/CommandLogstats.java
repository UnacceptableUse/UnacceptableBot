package com.unacceptableuse.ircbot.command;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.concurrent.TimeUnit;

public class CommandLogstats extends Command
{

	public CommandLogstats(Object ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
			throws Exception
	{
		String args[] = message.split(" ");
		String channelToAnalyze = args.length > 1 ? args[1].startsWith("#") ? args[1] : "#"+args[1] : channel;
		
		File f = new File(channelToAnalyze+".log");
		if(!f.exists())
		{
			sendMessage(channel, "That channel hasn't been logged.");
			return;
		}else
		{		
			String firstMessage = "none?";
			int lines = 0;
			BufferedReader br = new BufferedReader(new FileReader(f));
			String line = "";
			while((line = br.readLine()) != null)
			{
				lines++;
				if(lines == 1)
					firstMessage = line;
			}
           
			
			sendMessage(channel, String.format("Log contains %s lines and is %sMB in size.",  lines, (f.length() / 1024) /1024));
			sendMessage(channel, "&BOLDTHE FIRST MESSAGE WAS:&RESET "+firstMessage);
			
		}

	}

	@Override
	public String[] getAliases()
	{
		
		return new String[]{"logstats"};
	}

	@Override
	public String getHelp()
	{
		
		return "[channel] - Shows stats about the current channel log";
	}

}
