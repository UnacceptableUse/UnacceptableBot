package com.unacceptableuse.ircbot.command;

public class CommandRPS extends Command
{

	public CommandRPS(Object ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
			throws Exception
	{
		if(ub.commandProgression.get(sender+":DogeTimeout") != null){sendMessage(channel,"You must wait &BOLD"+ub.commandProgression.get(sender+":DogeTimeout")+"&RESET seconds until you can play again!"); return;}
		
		
		String[] args = message.split(" ");
		if(args.length == 1){sendSyntax(channel);return;}
		
		//1 = ROCK 2 = PAPER 3 = SCISSORS
		
			int playerChoice = args[1].equalsIgnoreCase("rock") ? 1 : args[1].equalsIgnoreCase("paper") ? 2 : args[1].equalsIgnoreCase("scissors") ? 3 : 0;
			if(playerChoice == 0){sendSyntax(channel);return;}else
			{
				int steveChoice = (ub.rand.nextInt(300)/100)+1;
				boolean playerWon = playerChoice == steveChoice ? false : playerChoice == 1 && steveChoice == 2 ? false : playerChoice == 3 && steveChoice == 1 ? false : playerChoice == 2 && steveChoice == 3 ? false :  true; 
				sendMessage(channel, (playerWon ? "You WIN! " : steveChoice == playerChoice ? "You TIED! " : "You LOSE! ")+ub.BOT_NAME+" chose "+getAction(steveChoice)+" and you chose "+getAction(playerChoice));
				if(playerWon && !ub.disabledCommands.contains("rpsrewards"))
					sendMessage(channel, ".tip "+sender+" "+ub.rand.nextInt(11)+1);
			}
			ub.commandProgression.put(sender+":RPSTimeout", 60);
		
	}
	
	private String getAction(int num)
	{
		switch(num)
		{
		case 1:
			return "rock";
		case 2:
			return "paper";
		case 3:
			return "scissors";
		default:
			return "error ("+num+")";
		}
	}

	@Override
	public String[] getAliases()
	{
		
		return new String[]{"rps"};
	}

	@Override
	public String getHelp()
	{
		
		return " rock/paper/scissors - Play rock paper scissors against steve";
	}

}
