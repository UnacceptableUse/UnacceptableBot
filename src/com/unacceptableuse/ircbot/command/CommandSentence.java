// $codepro.audit.disable commandExecution
package com.unacceptableuse.ircbot.command;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class CommandSentence extends Command
{

	public CommandSentence(Object ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message) throws Exception
	{
		ArrayList<String> arr = new ArrayList<String>();
		String[] args = message.split(" ");
		
		
		if(args.length < 2){sendSyntax(channel);return;}
		
		Process p = Runtime.getRuntime().exec("grep "+message.replace(args[0]+" ", "").toLowerCase()+" "+channel+".log "+(args.length > 2 ? args[2] : ""));
		BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
		
		String line ="";
		while((line = br.readLine()) != null)
		{
			arr.add(line);
		}
		
		if(arr.size() == 0){sendMessage(channel, "Unable to find a sentence containing that!");return;}
		
		sendMessage(channel, arr.size()+" occurances of "+message.replace(args[0]+" ", "")+". Random sentence below:");
		sendMessage(channel, arr.get(ub.rand.nextInt(arr.size())));

	}

	@Override
	public String[] getAliases()
	{
		
		return new String[]{"sentence","sentance","randomsentence"};
	}

	@Override
	public String getHelp()
	{
		
		return "<word> - Produces a random sentence containing the word";
	}
	
	@Override
	public boolean doesAnnoyTnt64()
	{
		return true;
	}

}
