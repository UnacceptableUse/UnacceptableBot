package com.unacceptableuse.ircbot.command;

import java.io.IOException;

import com.unacceptableuse.ircbot.UnacceptableBot;

public class CommandForceLoad extends Command
{

	public CommandForceLoad(UnacceptableBot ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
	{
		try
		{
			ub.loadThings();
		} catch (IOException e)
		{
			sendMessage(channel, "IOException: "+e.getMessage());
			e.printStackTrace();
		}
		sendMessage(channel, "Done!");
	}

	@Override
	public String[] getAliases()
	{
		return new String[]{"forceload"};
	}

	@Override
	public String getHelp()
	{
		return " - Forces "+ub.BOT_NAME+" to load all settings.";
	}

}
