package com.unacceptableuse.ircbot.command;

import com.unacceptableuse.ircbot.UnacceptableBot;

public class CommandSilentConnect extends Command
{

	public CommandSilentConnect(UnacceptableBot ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
	{
		String[] args = message.split(" ");
		if(!ub.aloudToMess){sendMessage(channel, "This is why we can't have nice things");return;}
		sendMessage(channel, "Connected to channel "+args[1]);
		if(args.length <= 2)
			ub.channel(args[1], true);
		else
			ub.channel(args[1], args[2], true);
	}

	@Override
	public String[] getAliases()
	{
		return new String[]{"silentconnect"};
	}

	@Override
	public String getHelp()
	{
		return "<channel> (pass) - Connects to a channel without sending join message";
	}

}
