package com.unacceptableuse.ircbot.command;

import com.unacceptableuse.ircbot.UnacceptableBot;

public class CommandDisconnect extends Command
{

	public CommandDisconnect(UnacceptableBot ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
	{
		String[] args = message.split(" ");
		if(!ub.aloudToMess){sendMessage(channel, "This is why we can't have nice things");return;}
		sendMessage(channel, "Disconnected from channel "+args[1]);
		ub.partChannel(args[1], sender+" quit by command.");
	}

	@Override
	public String[] getAliases()
	{
		return new String[]{"disconnect"};
	}

	@Override
	public String getHelp()
	{
		return "<channel> - Leaves a channel";
	}

}
