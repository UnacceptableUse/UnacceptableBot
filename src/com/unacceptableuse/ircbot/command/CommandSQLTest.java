package com.unacceptableuse.ircbot.command;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.Properties;

public class CommandSQLTest extends Command
{


	public CommandSQLTest(Object ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
			throws Exception
	{
		
		sendMessage(channel, "Setting sqltest = sqlreturn: "+ub.sql.setSetting("sqltest","sqlreturn"));
		sendMessage(channel, "Reading setting: sqltest = "+ub.sql.getSetting("sqltest"));

		
	}

	@Override
	public String[] getAliases()
	{
		
		return new String[]{"sqltest"};
	}

	@Override
	public String getHelp()
	{
		
		return "tests the connection to the MySQL database";
	}

}
