package com.unacceptableuse.ircbot.command;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.Currency;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.unacceptableuse.ircbot.UnacceptableBot;

public class CommandDoge extends Command
{

	public CommandDoge(UnacceptableBot ub)
	{
		super(ub);
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message) throws Exception
	{
		String[] args = message.split(" ");
		
		boolean showExchanges = false;

		if(args.length > 1){
		if(args[1].equalsIgnoreCase("atmalik")){sendMessage(channel, "1 ATMALIK = 1907 DOGE");return;}
		if(args[1].equalsIgnoreCase("jokor")){sendMessage(channel, "1 JOKOR = 1337.420 DOGE");return;}

		args[1] = new BufferedReader(new InputStreamReader(ub.getUrlContents("http://fightthetoast.co.uk/assets/wordtonumber.php?word="+args[1]))).readLine();
		
		if(args.length > 2)
			if(args[2].equalsIgnoreCase("showexchanges"))
				showExchanges = true;		
		}

		try{
		com.google.gson.JsonParser parser = new com.google.gson.JsonParser();
		com.google.gson.JsonObject dogeElement = parser.parse(new InputStreamReader(ub.getUrlContents("https://chain.so/api/v2/get_price/DOGE"))).getAsJsonObject();
		JsonArray priceArray = dogeElement.get("data").getAsJsonObject().get("prices").getAsJsonArray();
		
		StringBuilder stb = new StringBuilder();
		
		stb.append((args.length > 1 ? args[1] : "1") + " DOGE = ");
		
		for(JsonElement jo : priceArray)
		{
			JsonObject object = jo.getAsJsonObject();
			
			double price = args.length > 1 ? Double.parseDouble(object.get("price").getAsString()) * Double.parseDouble(args[1]) : Double.parseDouble(object.get("price").getAsString());
			DecimalFormat df = new DecimalFormat("#");
			df.setMaximumFractionDigits(8);
			df.setCurrency(Currency.getInstance("USD"));
			if(price > 0)
			stb.append(df.format(price)+" "+object.get("price_base")+(showExchanges ? " "+object.get("exchange")+" | " : " | "));
		}
		

		sendMessage(channel, stb.toString().replace("\"",""));
		}catch(NumberFormatException e)
		{
			sendSyntax(channel);
		}
		
		
		
//		if(args[1].toLowerCase().equals("btc"))
//		{
//			com.google.gson.JsonParser parser = new com.google.gson.JsonParser();
//			com.google.gson.JsonObject dogeElement = parser.parse(new InputStreamReader(ub.getUrlContents("http://www.cryptocoincharts.info/v2/api/tradingPair/DOGE_BTC"))).getAsJsonObject();
//			String exchangeRate = dogeElement.get("price").getAsString();
//			sendMessage(channel, "1 DOGE = "+exchangeRate+" BTC");
//		}else
//		{
//			BufferedReader br = new BufferedReader(new InputStreamReader(ub.getHTTPSUrlContents("https://moolah.io/api/rates?f="+args[1].toUpperCase()+"&t=DOGE&a=1")));
//			sendMessage(channel, "1 "+args[1].toUpperCase()+" = "+br.readLine()+" DOGE");
//		}
	}

	@Override
	public String[] getAliases()
	{
		return new String[]{"doge"};
	}

	@Override
	public String getHelp()
	{
		return " [amount] - Shows DOGE conversion to other coins";
	}

}
