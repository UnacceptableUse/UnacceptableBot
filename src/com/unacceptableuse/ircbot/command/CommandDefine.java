package com.unacceptableuse.ircbot.command;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class CommandDefine extends Command
{
	public String multiString;
	public InputStream is;
	
	public CommandDefine(Object ub)
	{
		super(ub);
		
	}

	//http://api.wordnik.com:80/v4/word.json/hello/definitions?limit=1&includeRelated=false&sourceDictionaries=webster&useCanonical=false&includeTags=false&api_key=0e73155eb6a8493af441503d98e0bdd1d2f36bf63a22d5c12
	
	@Override
	public void excecuteCommand(String channel, String sender, String message) throws Exception
	{
		String[] args = message.toLowerCase().split(" ");
		
		if(args.length < 2)
		{
			sendSyntax(channel);
			return;
		}
		
		if(args[0].equals("!defineud"))
		{
			return;
		}
		
		StringBuilder sb = new StringBuilder();
		
		if(args.length > 2)
		{
			for(int i = 1; i < args.length; i++)
			{
				if(sb.toString().equals(""))
				{
					sb.append(args[i]);
				}
				else
				{
					sb.append("%20" + args[i]);
				}
			}
			multiString = sb.toString();
		}
		else
		{
			multiString = args[1];
		}
		
		System.out.println();
		
		try {
			
			URL url = new URL("http://api.wordnik.com:80/v4/word.json/" + multiString + "/definitions?limit=200&includeRelated=false&sourceDictionaries=all&useCanonical=false&includeTags=false&api_key=0e73155eb6a8493af441503d98e0bdd1d2f36bf63a22d5c12");
			
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			is =  conn.getInputStream();
 
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		JsonParser json = new JsonParser();
		JsonArray defineArray = json.parse(new InputStreamReader(is)).getAsJsonArray();
		
		JsonObject jsonObj = new JsonObject();
		jsonObj.add("defineArray", defineArray);
		
		try
		{
			sendMessage(channel,
						String.format("%s: %s",
						jsonObj.getAsJsonObject().get("defineArray").getAsJsonArray().get(0).getAsJsonObject().get("word").toString().replace("\"",""),
						jsonObj.getAsJsonObject().get("defineArray").getAsJsonArray().get(0).getAsJsonObject().get("text").toString().replace("\"","")));
		} catch (IndexOutOfBoundsException e)
		{	
			if(args.length > 2)
			{
				for(int i = 1; i < args.length; i++)
				{
					if(sb.toString().equals(""))
					{
						sb.append(args[i]);
					}
					else
					{
						sb.append("+" + args[i]);
					}
				}
				multiString = sb.toString();
			}
			else
			{
				multiString = args[1];
			}
			
			try {
				
				URL url = new URL("http://api.urbandictionary.com/v0/define?term=" + multiString);
				
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				is =  conn.getInputStream();
	 
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			
			JsonObject udObject = json.parse(new InputStreamReader(is)).getAsJsonObject();
			try
			{
				sendMessage(channel,
							String.format("%s: %s",
							multiString.replace("+", " "),
							udObject.get("list").getAsJsonArray().get(0).getAsJsonObject().get("definition").toString().replace("\"","")));
			} catch(IndexOutOfBoundsException e3)
			{
				sendMessage(channel, "Word not found!");
			}
		}
	}

	@Override
	public String[] getAliases()
	{		
		return new String[]{"define"};
	}

	@Override
	public String getHelp()
	{
		
		return "<word> - Defines a word";
	}

}
