package com.unacceptableuse.ircbot.command;

import com.wolfram.alpha.WAEngine;
import com.wolfram.alpha.WAException;
import com.wolfram.alpha.WAPlainText;
import com.wolfram.alpha.WAPod;
import com.wolfram.alpha.WAQuery;
import com.wolfram.alpha.WAQueryResult;
import com.wolfram.alpha.WASubpod;

public class CommandWolfram extends Command
{

	public CommandWolfram(Object ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
	{
		String[] args = message.split(" ");
		if(args.length < 2){sendSyntax(channel);return;}
		
		sendMessage(channel, "Working...");
		
		WAEngine wolfram = new WAEngine();
		
		wolfram.setAppID("V4GHYA-AJTVGY4XJQ");
		WAQuery query = wolfram.createQuery();
		query.setInput(message.replace(args[0]+" ", ""));
		int max = 10, count = 0;
		
		try{
			
			WAQueryResult result = wolfram.performQuery(query);
			
			if(result.isError())
				sendMessage(channel, "Query error: "+result.getErrorCode()+": "+result.getErrorMessage());
			else if(!result.isSuccess())
				sendMessage(channel, "Query was not understood.");
			else
				for(WAPod pod : result.getPods())
				{	
					if(!pod.isError())
						for(WASubpod subpod : pod.getSubpods())
							for(Object obj : subpod.getContents())
								if(obj instanceof WAPlainText && ((WAPlainText)obj).getText().length() > 1)
									sendMessage(channel, " "+(pod.getTitle().length() > 1 ? "&BOLD"+pod.getTitle()+"&RESET: " : "")+((WAPlainText)obj).getText());
					count++;
					if(count > max)
						return;
					
				}
		}catch(WAException e)
		{
			sendMessage(channel,"An error ocurred performing this query: "+e.getMessage());
		}
	}

	@Override
	public String[] getAliases()
	{
		
		return new String[]{"wolfram"};
	}

	@Override
	public String getHelp()
	{
		return "all/result name <query> - Performs wolfram alpha query";
	}

}
