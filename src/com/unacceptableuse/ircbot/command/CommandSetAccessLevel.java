package com.unacceptableuse.ircbot.command;

import com.unacceptableuse.ircbot.UnacceptableBot;

public class CommandSetAccessLevel extends Command
{

	public CommandSetAccessLevel(UnacceptableBot ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
	{
		String[] args = message.split(" ");
		if(args.length < 2){
			sendSyntax(channel);
			return;
		}
		try{
			if(Integer.parseInt(args[2]) > ub.getAccessLevel(sender))
				sendMessage(channel, "&REDYou can't set someone's access level higher than your own!");
			else
			{
				ub.accessLevels.put(args[1].toLowerCase(), Integer.parseInt(args[2]));
				sendMessage(channel, "&BOLD"+args[1]+"&RESET's access level is now &BOLD"+ub.getAccessLevel(args[1]));
			}
				
		}catch(Exception e)
		{
			sendMessage(channel, "&REDInvalid access level "+args[2]);
		}
	}

	@Override
	public String[] getAliases()
	{
		return  new String[]{"setaccesslevel","accesslevel"};
	}

	@Override
	public String getHelp()
	{	
		return"<user> <level> - Set someones access level";
	}
	
	@Override
	public int getAccessLevel()
	{
		return 2;
	}

}
