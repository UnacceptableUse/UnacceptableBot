package com.unacceptableuse.ircbot.command;

import com.unacceptableuse.ircbot.UBStatic;
import com.unacceptableuse.ircbot.ValueFormatter;

public class CommandYoMamma extends Command
{

	public CommandYoMamma(Object ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
	{
		sendMessage(channel, generateYoMammaJoke());
	}
	
	
	private String generateYoMammaJoke()
	{
		
		return String.format("Yo mamma so %s that %s %s!",ValueFormatter.chooseFromArray(UBStatic.adj), ValueFormatter.chooseFromArray(UBStatic.action),ValueFormatter.chooseFromArray(UBStatic.dig));
	}

	@Override
	public String[] getAliases()
	{
		return new String[]{"yomamma","yomomma","yourmumjoke","yourmum","ym"};
	}

	@Override
	public String getHelp()
	{
		return "- Returns a randomly generated Yo Mamma joke";
	}
	
	@Override
	public boolean doesAnnoyTnt64()
	{
		return true;
	}

}
