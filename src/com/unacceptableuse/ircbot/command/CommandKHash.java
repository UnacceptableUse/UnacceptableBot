package com.unacceptableuse.ircbot.command;

import java.io.InputStreamReader;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class CommandKHash extends Command
{
	
	public CommandKHash(Object ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message) throws Exception
	{
		String[] args = message.toLowerCase().split(" ");
		
		if(args.length < 4)
		{
			sendSyntax(channel);
			return;
		}
		
		if(!args[3].equals("minute") && !args[3].equals("hour") && !args[3].equals("day") && !args[3].equals("week") && !args[3].equals("month") && !args[3].equals("year"))
		{
			sendSyntax(channel);
			return;
		}

		JsonParser json = new JsonParser();
		JsonObject kHashObject = json.parse(new InputStreamReader(ub.getHTTPSUrlContents("https://coinking.io/api.php?key=c639476cfa3a8ab1bdf8a1532d7c5ddb&type=hashtocoins&coin="+args[2]+"&khash=" + args[1].replace("gh","000000").replace("th","000000000") + "&output=json"))).getAsJsonObject();
		sendMessage(channel, kHashObject.get("per" + args[3]).getAsString().equals("0") ? "Invalid coin or not enough hashes" : 
					String.format("Expected Profit: &BOLD%s&RESET %s per &BOLD%s&RESET",
					kHashObject.get("per" + args[3]).getAsString(), args[2].toUpperCase(),
					args[3].toString()));
	}

	@Override
	public String[] getAliases()
	{
		return new String[]{"khash"};
	}

	@Override
	public String getHelp()
	{
		return "<khashes> <coin> <minute/hour/day/week/month/year> - Calculates predicted profit for mining speed.";
	}

}
