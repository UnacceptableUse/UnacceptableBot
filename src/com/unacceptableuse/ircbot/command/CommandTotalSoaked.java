package com.unacceptableuse.ircbot.command;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.Currency;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class CommandTotalSoaked extends Command
{

	public CommandTotalSoaked(Object ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
			throws Exception
	{

		String args[] = message.split(" ");
		String soaker = args.length > 1 ? args[1] : sender;
		
		File f = new File(channel+".log");
		{		
			long total = 0L;
			int count = 0;
			BufferedReader br = new BufferedReader(new FileReader(f));
			String line = "";
			while((line = br.readLine()) != null)
			{
				if(line.contains(soaker+" is soaking") && line.contains("<DogeWallet> "))
				{
					String totalStr = line.split("with")[1].split("Doge")[0].trim();
					total+= Long.parseLong(totalStr);
					count++;
				}
			}
			
//				com.google.gson.JsonParser parser = new com.google.gson.JsonParser();
//				com.google.gson.JsonObject dogeElement = parser.parse(new InputStreamReader(ub.getUrlContents("http://www.cryptocoincharts.info/v2/api/tradingPair/DOGE_USD"))).getAsJsonObject();
//				
//				long amt = dogeElement.getAsJsonObject().get("price").getAsLong()*total;
//				
//			
//			
			
           
			sendMessage(channel, soaker+" has soaked "+count+" times, a total of "+total+" DOGE.");
			
			
		}
	}

	@Override
	public String[] getAliases()
	{
		
		return new String[]{"totalsoaked","soaktotal"};
	}

	@Override
	public String getHelp()
	{
		
		return "no need for help here";
	}

}
