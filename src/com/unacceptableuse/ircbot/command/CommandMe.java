package com.unacceptableuse.ircbot.command;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.jibble.pircbot.User;

public class CommandMe extends Command
{

	public CommandMe(Object ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
			throws Exception
	{
		String[] args = message.split(" ");
		File[] files = new File("./").listFiles();
		ArrayList<File>logs = new ArrayList<File>();
		String personToInvestigate = args.length > 1 ? args[1] : sender;
		for(File f : files)
		{
			if(f.isDirectory() || !f.canRead())continue;
			if(f.getName().startsWith("#") && !f.getName().contains("copy"))
			{
				logs.add(f);
			}
			
		}
		
		
		int count = 0;
		HashMap<String, Integer> wordUsage = new HashMap<String, Integer>();

		
		for(File log : logs)
		{
			BufferedReader br = new BufferedReader(new FileReader(log));
			String line = br.readLine();
			while((line = br.readLine()) != null)
			{
				if(line.contains("<"+personToInvestigate+">")){
					count++;
					String[] words = line.replaceAll("([^a-zA-Z ]+)","").split(" ");
					for(String s : words)
					{
						String ls = s.toLowerCase();
						if(wordUsage.containsKey(ls))
							wordUsage.put(ls, wordUsage.get(ls)+1);
						else
							wordUsage.put(ls, 1);
					}
				}
			}
		}

		
		String mostMentionedUser = null; 
		Iterator<String> it = wordUsage.keySet().iterator();
		while(it.hasNext())
		{
			String s = it.next();
//			if(s.length() < 2 )
//			{
//				wordUsage.remove(s);
//			}
			for(User user : ub.getUsers(channel))
			{
				if(s.equalsIgnoreCase(user.getNick()))
				{
					if(mostMentionedUser == null || wordUsage.get(s) > wordUsage.get(mostMentionedUser.toLowerCase()) && !s.equalsIgnoreCase(personToInvestigate))
					{
						mostMentionedUser = s;
					}
				}
			}
			
		}
		
		
		sendMessage(channel, wordUsage.size()+" different words over "+count+" messages in "+logs.size()+" channels. (This isn't the command's function this is debugging)");
		sendMessage(channel, "Most mentioned user was "+mostMentionedUser+" with "+wordUsage.get(mostMentionedUser)+" uses.");
		
		
		
		
	}

	@Override
	public String[] getAliases()
	{
		return new String[]{"investigate"};
	}

	@Override
	public String getHelp()
	{
		
		return "<user> - finds things out about a user from the channels it is in";
	} 

}
