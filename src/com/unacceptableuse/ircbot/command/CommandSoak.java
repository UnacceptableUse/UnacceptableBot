package com.unacceptableuse.ircbot.command;

public class CommandSoak extends Command
{

	public CommandSoak(Object ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
			throws Exception
	{
		if(ub.dogeBalance2 < 100)
		{
			sendMessage(channel, "Not enough doge to soak");
		}else
		{
			sendMessage("DogeWallet",".active");
		}
			
	}

	@Override
	public String[] getAliases()
	{
		
		return new String[]{"soak"};
	}

	@Override
	public String getHelp()
	{
		
		return "Automatic soak";
	}

}
