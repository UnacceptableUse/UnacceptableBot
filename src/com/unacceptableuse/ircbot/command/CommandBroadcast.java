package com.unacceptableuse.ircbot.command;

public class CommandBroadcast extends Command
{

	public CommandBroadcast(Object ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
			throws Exception
	{
		for(String s : ub.getChannels())
			sendMessage(s, message.replace("!broadcast ", "BROADCAST FROM "+sender+": "));
	}

	@Override
	public String[] getAliases()
	{
		
		return new String[]{"broadcast"};
	}

	@Override
	public String getHelp()
	{
		
		return null;
	}
	
	@Override
	public int getAccessLevel()
	{
		return 4;
	}

}
