package com.unacceptableuse.ircbot.command;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

import org.pircbotx.User;

import com.unacceptableuse.ircbot.UnacceptableBot;
import com.unacceptableuse.ircbot.ValueComparator;

public class CommandSoMuchStats extends Command
{

	public CommandSoMuchStats(UnacceptableBot ub)
	{
		super(ub);
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message) throws Exception
	{
		sendMessage(channel, "Analysing...");
		String[] args = message.split(" ");
		
		File logs = null;
		if(args.length > 1)
			logs = new File(args[1]+".log");
		else
			logs = new File(channel+".log");
		
		BufferedReader br = new BufferedReader(new FileReader(logs));
		
		final String[] swears = {"fuck", "shit", "wank", "fap", "fag", "faggot", "masturbate", "boob", "tit", "bastard", "cunt", "damn", "penis", "dick", "anus", "arse", "ass", "arsehole", "asshole", "vagina", "clit", "clusterfuck", "bitch", "blowjob", "bollocks", "balls", "boner", "motherfucker", "woover", "gangbang", "bang"};
		Integer[] dayUsage = {0, 0, 0, 0, 0, 0, 0};
		int mostFrequentDay = 0;
		int swearCount = 0;
		int points = 0;
		int totalWords = 0;
		float lines = 0;
		HashMap<String, Integer> wordUsage = new HashMap<String, Integer>();
		HashMap<String, Integer> purgedWords = new HashMap<String, Integer>();
		ValueComparator comparator = new ValueComparator(wordUsage);
		TreeMap<String, Integer> sortedWords = new TreeMap<String, Integer>(comparator);
		StringBuilder fullContent = new StringBuilder();
		String mostMentionedUser = "UnacceptableUse";
		String paste = "Wow this message should never appear";
		String wordsGraph = "These arn't the graphs you are looking for";
		String daysGraph = "Welp";
		String nickGraph = "Such broken";
		String mostNicks = "";


	
		try{
		
			String line = "";
			while((line = br.readLine()) != null)
			{
				lines++;
				
				if(line.startsWith("[Sun"))dayUsage[0]++;
				if(line.startsWith("[Mon"))dayUsage[1]++;
				if(line.startsWith("[Tue"))dayUsage[2]++;
				if(line.startsWith("[Wed"))dayUsage[3]++;
				if(line.startsWith("[Thu"))dayUsage[4]++;
				if(line.startsWith("[Fri"))dayUsage[5]++;
				if(line.startsWith("[Sat"))dayUsage[6]++;
				
				
				line = line.replaceAll("(\\[(.*?)\\]|\\<(.*?)\\>)", "").replaceAll("([^a-zA-Z ]+)",""); // From this point on all operations use the message and not the sender or time/date or any punctuation
				
				String[] words = line.split(" ");		
				for(String s : words)
				{
					totalWords++;
					s = s.toLowerCase();
					
					if(s.length() > 0) //Gets rid of all blank words that my have slipped through
					{
						if(s.length() > 0) //Gets rid of all blank words that my have slipped through
							wordUsage.put(s, wordUsage.containsKey(s) ? wordUsage.get(s)+1 : 1);
						
						for(String swear : swears)
							if(s.equals(swear))
								swearCount++;
					}
				}
				
			}
			
			br.close();
			
			
	        purgedWords.putAll(wordUsage);	
		      
	        purgedWords.remove("the");
	        
	        purgedWords.remove("the");
	        purgedWords.remove("i");
	        purgedWords.remove("be");
	        purgedWords.remove("to");
	        purgedWords.remove("of");
	        purgedWords.remove("and");
	        purgedWords.remove("a");
	        purgedWords.remove("in");
	        purgedWords.remove("that");
	        purgedWords.remove("have");
	        purgedWords.remove("it");
	        purgedWords.remove("for");
	        purgedWords.remove("not");
	        purgedWords.remove("on");
	        purgedWords.remove("with");
	        purgedWords.remove("he");
	        purgedWords.remove("as");
	        purgedWords.remove("you");
	        purgedWords.remove("do");
	        purgedWords.remove("at");
	        purgedWords.remove("this");
	        purgedWords.remove("but");
	        purgedWords.remove("his");
	        purgedWords.remove("by");
	        purgedWords.remove("from");
	        purgedWords.remove("they");
	        purgedWords.remove("we");
	        purgedWords.remove("say");
	        purgedWords.remove("her");
	        purgedWords.remove("she");
	        purgedWords.remove("or");
	        purgedWords.remove("an");
	        purgedWords.remove("will");
	        purgedWords.remove("my");
	        purgedWords.remove("one");
	        purgedWords.remove("all");
	        purgedWords.remove("would");
	        purgedWords.remove("there");
	        purgedWords.remove("their");
	        purgedWords.remove("what");
	        purgedWords.remove("so");
	        purgedWords.remove("up");
	        purgedWords.remove("out");
	        purgedWords.remove("if");
	        purgedWords.remove("about");
	        purgedWords.remove("who");
	        purgedWords.remove("get");
	        purgedWords.remove("which");
	        purgedWords.remove("go");
	        purgedWords.remove("me");
	        purgedWords.remove("when");
	        purgedWords.remove("make");
	        purgedWords.remove("can");
	        purgedWords.remove("like");
	        purgedWords.remove("time");
	        purgedWords.remove("no");
	        purgedWords.remove("just");
	        purgedWords.remove("him");
	        purgedWords.remove("know");
	        purgedWords.remove("take");
	        purgedWords.remove("people");
	        purgedWords.remove("into");
	        purgedWords.remove("year");
	        purgedWords.remove("your");
	        purgedWords.remove("good");
	        purgedWords.remove("some");
	        purgedWords.remove("could");
	        purgedWords.remove("them");
	        purgedWords.remove("see");
	        purgedWords.remove("other");
	        purgedWords.remove("than");
	        purgedWords.remove("then");
	        purgedWords.remove("now");
	        purgedWords.remove("look");
	        purgedWords.remove("only");
	        purgedWords.remove("come");
	        purgedWords.remove("its");
	        purgedWords.remove("over");
	        purgedWords.remove("think");
	        purgedWords.remove("also");
	        purgedWords.remove("back");
	        purgedWords.remove("after");
	        purgedWords.remove("use");
	        purgedWords.remove("two");
	        purgedWords.remove("how");
	        purgedWords.remove("our");
	        purgedWords.remove("work");
	        purgedWords.remove("first");
	        purgedWords.remove("well");
	        purgedWords.remove("way");
	        purgedWords.remove("even");
	        purgedWords.remove("new");
	        purgedWords.remove("want");
	        purgedWords.remove("because");
	        purgedWords.remove("any");
	        purgedWords.remove("these");
	        purgedWords.remove("give");
	        purgedWords.remove("day");
	        purgedWords.remove("most");
	        purgedWords.remove("us");  
	        purgedWords.remove("is"); 

			
	        sortedWords.putAll(purgedWords);
	        
	        
	        
	        
	        
	        HashMap<String, Integer> majorityWords = new HashMap<String,Integer>();
	        
	        for(String s : wordUsage.keySet())
	        {
	        	if(wordUsage.get(s) > 10 && !s.equals("i") && !s.equals("and") && !s.equals("a") && !s.equals("the") && !s.equals("be") && !s.equals("of") && !s.equals("and") && !s.equals("in") && !s.equals("on") && !s.equals("he") && !s.equals("she"))
	        		majorityWords.put(s, wordUsage.get(s));
	        		
	        	if(wordUsage.get(s) == 69)
	        		points+=1000;
	        	
	        	ub.wordUsage.put(s, wordUsage.get(s));
	        }

	        
	             
//	        for(org.jibble.pircbot.User user: ub.getUsers(channel))
//	        {
//	        	if(wordUsage.get(mostMentionedUser.toLowerCase()) == null)
//	        	{
//	        		mostMentionedUser = user.getNick();
//	        	}
//	        	else
//		        	if(wordUsage.get(user.getNick().toLowerCase()) > wordUsage.get(mostMentionedUser.toLowerCase()))
//		        		mostMentionedUser = user.getNick();
//	        }

		        	
		        		
	        
	        for(String s : sortedWords.keySet())
	        		fullContent.append(s + ": " + wordUsage.get(s));
	        
	        
	        
    
	                
	       // paste = ub.uploadPaste(fullContent.toString(), sender, channel);
	        paste = "TODO: fix paste uploading"; //TODO: THIS
	        
	        
	        if(!args[0].startsWith("!analy"))
	        wordsGraph = ub.generateStatsGraph(sortedWords);
	        
	        fullContent = new StringBuilder();
	        
	        for(int d = 0; d < dayUsage.length; d++)
	        {
	        	
	        	if(dayUsage[d] > dayUsage[mostFrequentDay])
	        		mostFrequentDay = d;
	        	
	        	fullContent.append((d == 0 ? "Sun" : d == 1 ? "Mon" : d == 2 ? "Tues" : d == 3 ? "Wed" : d == 4 ? "Thur" : d == 5 ? "Fri" : "Sat")+"="+dayUsage[d]);
	    		if(d < 6)
	    			fullContent.append('&');
	        	
	        }
	 
	        
	        
	        
	        if(!args[0].startsWith("!analy"))
	        {
	        	daysGraph = ub.generateGraph(fullContent.toString());
	        	
	        	for(String usrn : ub.pastNicks.keySet())
	        	{
	        		System.out.println("User "+usrn+" has changed their nickname "+ub.pastNicks.get(usrn).size());
	        		if(ub.pastNicks.get(mostNicks) == null)
	        			mostNicks = usrn;
	        		else
	        		if(ub.pastNicks.get(usrn).size() > ub.pastNicks.get(mostNicks).size())
	        			mostNicks = usrn;
	        	}
	        	
	        	nickGraph = ub.generateStatsGraph(ub.messagesSent);

	        }
	        
	        
	        
	        //Calculate points
	        points+=swearCount/2;
	        points+=mostFrequentDay*10;
	        points+=Math.cos(lines);
	        points+=lines/10;
	        points+=channel.length();
	        points+=ub.rand.nextInt(100);
	        points+=ub.boobcount*100;
	        points+=wordUsage.size()*10;
	        points+=wordUsage.toString().length();
	        points-=wordUsage.get("lel")*1000;
	        points+=wordUsage.get("fuck")*10;
	        points+=wordUsage.get("awesome")*2;
	        
		}catch(Exception e){
			sendMessage(channel, "Note: Some stats will not display, something went wrong. ("+e.getMessage()+")");
			e.printStackTrace();
		}finally
		{
			ub.setMessageDelay(100);
			if(!args[0].startsWith("!analy"))
			{
				sortedWords.remove(sortedWords.firstEntry().getKey());
				
				sendMessage(channel, String.format("In &BOLD%s&RESET messages, &BOLD%s&RESET different words were used. An average of &BOLD%s&RESET different words per message.", lines, wordUsage.size(), (wordUsage.size()/lines)));
				sendMessage(channel, String.format("&BOLD%s&RESET total words were used. An average of &BOLD%s&RESET words per message.", totalWords, totalWords/lines));
		        sendMessage(channel, String.format("The most used word is &BOLD%s&RESET with &BOLD%s&RESET uses.", sortedWords.firstEntry().getKey(), wordUsage.get(sortedWords.firstEntry().getKey())));
		        sendMessage(channel, String.format("&BOLD%s&RESET because &BOLD%s&RESET messages were sent on &BOLD%s&RESET.", mostFrequentDay == 0 ? "You're all bible bashers" : mostFrequentDay == 1 ? "Nobody hates mondays" : mostFrequentDay == 2 ? "Joel's hard" : mostFrequentDay == 3 ? "The middle of the week is best" : mostFrequentDay == 4 ? "There's something about Thursdays" : mostFrequentDay == 5 ? "7AM wakin' up in the mornin'" : "IRC is the best hangover cure", dayUsage[mostFrequentDay], mostFrequentDay == 0 ? "Sunday" : mostFrequentDay == 1 ? "Monday" : mostFrequentDay == 2 ? "Tuesday" : mostFrequentDay == 3 ? "Wednesday" : mostFrequentDay == 4 ? "Thursday" : mostFrequentDay == 5 ? "Friday" : "Saturday"));
		        sendMessage(channel, String.format("&BOLD%s&RESET swears have been said%s", swearCount, wordUsage.get("fuck") > swearCount/2 ? " and you're all horny because "+wordUsage.get("fuck")+" out of "+swearCount+" were 'fuck'":"."));
		        sendMessage(channel, String.format("&BOLD%s&RESET is having an identity crisis because they changed their nickname to &BOLD%s&RESET different things.", mostNicks,ub.pastNicks.get(mostNicks).size()));
		        //sendMessage(channel, String.format("&BOLD%s&RESET is %s popular with &BOLD%s&RESET mentions", mostMentionedUser, wordUsage.get(mostMentionedUser) > 10 ? "extremely":"a little", wordUsage.get(mostMentionedUser)));
		        sendMessage(channel, String.format("FULL LIST: %s / GRAPHS: BY WORD: %s / BY DAY: %s / BY USER: %s", paste, wordsGraph, daysGraph, nickGraph));
			}
	        sendMessage(channel, String.format("Overall, &BOLD%s&RESET gets &BOLD%s&RESET points!", args.length > 2 ? args[2] : channel, points));
	        ub.setMessageDelay(1000);
		} 
	}

	@Override
	public String[] getAliases()
	{
		return new String[]{"analyze","analyse","somuchstats", "allstats"};
	}

	@Override
	public String getHelp()
	{
		
		return "[log] - Provides detailed statistics from the message log.";
	}
	
	@Override
	public boolean doesAnnoyTnt64()
	{
		return true;
	}

}
