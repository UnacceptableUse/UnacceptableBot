package com.unacceptableuse.ircbot.command;

import java.io.InputStreamReader;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class CommandLastBlock extends Command
{
	public CommandLastBlock(Object ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message) throws Exception
	{
		String[] args = message.toLowerCase().split(" ");
		
		if(args.length < 2)
		{
			sendSyntax(channel);
			return;
		}
		
		JsonParser json = new JsonParser();
		JsonObject lastBlockObject = json.parse(new InputStreamReader(ub.getHTTPSUrlContents("https://coinking.io/api.php?key=c639476cfa3a8ab1bdf8a1532d7c5ddb&type=lastblock&coin=" + args[1] + "&output=json"))).getAsJsonObject();
		sendMessage(channel,
					String.format("Block #: %s | Amount in Block: %s | Finder: %s | Confirmations: %s",
					lastBlockObject.get("height").toString().replace("\"",""),
					lastBlockObject.get("coins").toString().replace("\"",""),
					lastBlockObject.get("finder").toString().replace("\"",""),
					lastBlockObject.get("confirmations").toString().replace("\"","")));
	}

	@Override
	public String[] getAliases()
	{
		return new String[]{"lastblock"};
	}

	@Override
	public String getHelp()
	{
		return "<cointicker> - Get information on last block found.";
	}

}
