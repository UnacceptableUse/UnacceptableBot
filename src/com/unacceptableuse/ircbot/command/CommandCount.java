package com.unacceptableuse.ircbot.command;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.jibble.pircbot.User;

import com.unacceptableuse.ircbot.UnacceptableBot;

public class CommandCount extends Command
{

	public CommandCount(UnacceptableBot ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
	{
		String[] args = message.split(" ");
		if(args.length < 2){sendSyntax(channel);return;}
		boolean thisChannelOnly = false;
		if(args.length == 3 && args[2].equalsIgnoreCase("channelonly"))
			thisChannelOnly = true;
		
		try{
		int count = 0;
		File[] files = new File("./").listFiles();
		ArrayList<File>logs = new ArrayList<File>();
		for(File f : files)
		{
			if(f.isDirectory() || !f.canRead())continue;
			if(f.getName().startsWith("#") && !f.getName().contains("copy"))
			{
				if((thisChannelOnly && f.getName().contains(channel)) || !thisChannelOnly)	
					logs.add(f);
			}
					
			
		}

		for(File log : logs)
		{
			BufferedReader br = new BufferedReader(new FileReader(log));
			String line = br.readLine();
			while((line = br.readLine()) != null)
			{
				if(line.split(">")[1].toLowerCase().contains(args[1].toLowerCase()))
				{
					count++;
				}
			}
			br.close();
			
			
		}
		sendMessage(channel, args[1]+" has been said "+count+" times!");
		}catch(Exception e)
		{
			sendMessage(channel, "wow so error");
		}
	
		
	}

	@Override
	public String[] getAliases()
	{
		return new String[]{"count"};
	}

	@Override
	public String getHelp()
	{
		return " <word> - Counts how many times a word has been said.";
	}
	
	@Override
	public boolean doesAnnoyTnt64()
	{
		return true;
	}

}
