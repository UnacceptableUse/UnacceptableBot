package com.unacceptableuse.ircbot.command;

import com.unacceptableuse.ircbot.UnacceptableBot;

public class CommandStopRelay extends Command
{

	public CommandStopRelay(UnacceptableBot ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
	{
		sendMessage(channel, "No longer relaying");
		ub.relayChannel = null;
	}

	@Override
	public String[] getAliases()
	{
		return new String[]{"stoprelay"};
	}

	@Override
	public String getHelp()
	{	
		return " - Stops relaying from a channel";
	}

}
