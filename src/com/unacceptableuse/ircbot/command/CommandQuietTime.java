package com.unacceptableuse.ircbot.command;

import org.jibble.pircbot.User;

public class CommandQuietTime extends Command
{

	public CommandQuietTime(Object ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
			throws Exception
	{
		sendMessage(channel, "ok ok is quiet time now");
		ub.setMode(channel, "+m");
		for(User u : ub.getUsers(channel))
		{
			if(u.getNick().equals(ub.BOT_NAME))continue;
			ub.deOp(channel, u.getNick());
			ub.deVoice(channel, u.getNick());
			
		}
	}

	@Override
	public String[] getAliases()
	{
		
		return new String[]{"quiettime"};
	}

	@Override
	public String getHelp()
	{
		
		return "Is quiet time now";
	}

}
