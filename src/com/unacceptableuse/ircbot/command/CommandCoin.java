package com.unacceptableuse.ircbot.command;

import java.io.InputStreamReader;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class CommandCoin extends Command
{

	public CommandCoin(Object ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message) throws Exception
	{
		String[] args = message.toLowerCase().split(" ");
		
		if(args.length < 2)
		{
			sendSyntax(channel);
			return;
		}
		
		if(!args[1].equals("scrypt") && !args[1].equals("scryptn") && !args[1].equals("sha256"))
		{
			sendSyntax(channel);
			return;
		}
		
		JsonParser json = new JsonParser();
		JsonObject coinObject = json.parse(new InputStreamReader(ub.getHTTPSUrlContents("https://coinking.io/api.php?key=c639476cfa3a8ab1bdf8a1532d7c5ddb&type=current" + args[1] + "coin&output=json"))).getAsJsonObject();
		sendMessage(channel,
					String.format("%s is currently mining %s [%s] at difficulty %s",
					args[1],
					coinObject.get("name").toString().replace("\"",""),
					coinObject.get("nickname").toString().replace("\"",""),
					coinObject.get("difficulty").toString().replace("\"","")));
	}

	@Override
	public String[] getAliases()
	{
		return new String[]{"coin","currentcoin"};
	}

	@Override
	public String getHelp()
	{
		return "scrypt/scryptn/SHA256 - Gets the current coin being mined";
	}

}
