package com.unacceptableuse.ircbot.command;

public class CommandFunction extends Command
{

	public CommandFunction(Object ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
			throws Exception
	{
		String[] args = message.split(" ");
		if(args.length < 2){sendSyntax(channel);return;}
		
		ub.addCommand(new CommandDynamic(ub,args[1], message.replace(args[0]+" "+args[1]+" ","")));
		
		sendMessage(channel, "Added command !"+args[1]);
	}

	@Override
	public String[] getAliases()
	{
		
		return new String[]{"function"};
	}

	@Override
	public String getHelp()
	{
		
		return "<command> <function> - Create a simple command";
	}
	
	@Override
	public int getAccessLevel()
	{
		return 4;
	}

}
