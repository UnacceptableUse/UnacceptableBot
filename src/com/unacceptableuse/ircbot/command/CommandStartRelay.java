package com.unacceptableuse.ircbot.command;

import com.unacceptableuse.ircbot.UnacceptableBot;

public class CommandStartRelay extends Command
{

	public CommandStartRelay(UnacceptableBot ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
	{
		String[] args = message.split(" "); 
		if(!ub.aloudToMess){sendMessage(channel, "This is why we can't have nice things");return;}
		sendMessage(channel, "Now relaying from "+args[1]);
		ub.relayChannel = args[1];
		ub.relayTo = channel;
	}

	@Override
	public String[] getAliases()
	{
		return new String[]{"startrelay"};
	}

	@Override
	public String getHelp()
	{
		return " <channel> - Relays chat from another channel";
	}

}
