package com.unacceptableuse.ircbot.command;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import com.unacceptableuse.ircbot.UnacceptableBot;

public class CommandBalance extends Command
{

	public CommandBalance(UnacceptableBot ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
	{
		String[] args = message.split(" ");
		try{
			if(args.length < 2)
			{
				//sendSyntax(channel);
				return;
			}
			BufferedReader br = new BufferedReader(new InputStreamReader(ub.getUrlContents("http://dogechain.info/chain/Dogecoin/q/addressbalance/"+args[1])));
			sendMessage(channel, args[1]+" is worth "+br.readLine()+" DOGE");
		}catch(Exception e)
		{
			sendMessage(channel, "An error occurred: "+e.getMessage());
			e.printStackTrace();
		}
	}

	@Override
	public String[] getAliases()
	{
		return new String[]{"balance"};
	}

	@Override
	public String getHelp()
	{
		
		return " <address> - Returns the balance of a Dogecoin wallet.";
	}

}
