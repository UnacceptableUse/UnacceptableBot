package com.unacceptableuse.ircbot.command;

import com.unacceptableuse.ircbot.UnacceptableBot;

public class CommandBug extends Command
{

	public CommandBug(Object ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
			throws Exception
	{
		String[] args = message.split(" ");
		if(args.length < 2){sendSyntax(channel);return;}
		
		UnacceptableBot.logException("[REPORT] "+message.replace(args[0],""), new Exception());
		sendMessage(channel, "Bug has been logged.");
	}

	@Override
	public String[] getAliases()
	{
		return new String[]{"bug"};
	}

	@Override
	public String getHelp()
	{
		
		return "<bug> - Reports a bug";
	}

}
