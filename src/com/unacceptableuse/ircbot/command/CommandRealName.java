package com.unacceptableuse.ircbot.command;

import com.unacceptableuse.ircbot.UnacceptableBot;

public class CommandRealName extends Command
{

	public CommandRealName(UnacceptableBot ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
	{
		String[] args = message.split(" ");
		if(args[1] == null)sendSyntax(channel);
		sendMessage(channel, args[1]+"'s real name is "+ub.findRealName(args[1]));
	}

	@Override
	public String[] getAliases()
	{
		return new String[]{"realname","getrealname"};
	}

	@Override
	public String getHelp()
	{
		return " <username> - Returns the realname (if known)";
	}

}
