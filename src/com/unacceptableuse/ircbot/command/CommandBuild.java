package com.unacceptableuse.ircbot.command;

import com.unacceptableuse.ircbot.UnacceptableBot;

public class CommandBuild extends Command
{

	public CommandBuild(UnacceptableBot ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
	{
		sendMessage(channel, ub.BOT_NAME+" "+UnacceptableBot.version);
	}

	@Override
	public String[] getAliases()
	{
		
		return new String[]{"build", "version"};
	}

	@Override
	public String getHelp()
	{
		
		return " - Displays the version of "+ub.BOT_NAME;
	}

}
