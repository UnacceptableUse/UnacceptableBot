package com.unacceptableuse.ircbot.command;

public class CommandFaucet extends Command
{

	public CommandFaucet(Object ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
	{
		if(ub.commandProgression.get(sender+":DogeTimeout") != null){sendMessage(channel,"You must wait &BOLD"+ub.commandProgression.get(sender+":DogeTimeout")+"&RESET seconds until you can receive again!"); return;}
		
	//	if(ub.dogeBalance < 10){sendMessage(channel, "&BOLD&REDInsufficient funds for meaningful tip. !tip Shibe_Wanders to add.");return;}
		
		
		if(ub.dogeBalance2 < 1)
		{
			sendMessage(channel, "Not enough funds to tip :(");
			return;
		}
		
		if(ub.dogeBalance2 < 100)
		{
			sendMessage(channel, "Faucet is running low. Consider donating :3");
		}
		
		
		int amount = ub.rand.nextInt(ub.faucetLimit > ub.dogeBalance2 ? (int)ub.dogeBalance2 : ub.faucetLimit);
		if(channel.equals("#doge-coin"))
			sendMessage(channel, ".tip "+sender+" "+amount);
		else
			sendMessage(channel, "!tip "+sender+" "+amount);
		
		ub.commandProgression.put(sender+":DogeTimeout", 480);
		
	}

	@Override
	public String[] getAliases()
	{
		return new String[]{"faucet", "freedoge", "dogefaucet"};
	}

	@Override
	public String getHelp()
	{		
		return "- Tips you random amounts of doge";
	}

}
