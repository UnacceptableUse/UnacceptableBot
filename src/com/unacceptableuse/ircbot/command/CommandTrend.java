package com.unacceptableuse.ircbot.command;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class CommandTrend extends Command
{

	public CommandTrend(Object ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
			throws Exception
	{
		String[] args = message.split(" ");
		if(args.length == 1){sendSyntax(sender);return;} //If the user supplies no arguments we send them the syntax and return
		boolean thisChannelOnly = message.contains("thischannelonly");
		boolean compressResults = true;
		
		ArrayList<Integer> count = new ArrayList<Integer>(); //The array that will hold the accumultive frequency graph to be sent to the server
		
		//Gather the log files
		File[] files = new File("./").listFiles(); //Get all the files in the working directory
		ArrayList<File>logs = new ArrayList<File>();
		long oldestFileTime = System.currentTimeMillis();
		for(File f : files)
		{
			if(f.isDirectory() || !f.canRead())continue; //If we can't open the file or it is a directory there is no point in opening it...
			if(f.getName().startsWith("#") && !f.getName().contains("copy")) //We only count channel logs and ignore "copy" logs
			{
				if((thisChannelOnly && f.getName().contains(channel)) || !thisChannelOnly)	 //If we are only counting this channel check if the log is for this channel
				{
					BasicFileAttributes attr = Files.readAttributes(f.toPath(), BasicFileAttributes.class); 
					long tme = attr.creationTime().toMillis(); //Read the creation time to get the oldest log
					if(oldestFileTime > tme)
						oldestFileTime = tme;
				}
					logs.add(f); //Add the log to the array
			}
			
		}
		
		//For every log file we counted
		int linesRead = 0; //The total lines we have counted
		int lastCount = 0; //The amount of lines in the last log we counted
		for(File log : logs)
		{
			int lineNum = 0; //The number of the line we are on
			int lineInterval = 0; //The interval for compressed counting
			BufferedReader br = new BufferedReader(new FileReader(log));
			String line = br.readLine();
			while((line = br.readLine()) != null)
			{
				linesRead++;
				lineNum++;
				if(line.split(">")[1].toLowerCase().contains(args[1].toLowerCase()))
				{
					lastCount++; //Only count lines that contain the word we are looking for after the username
				}
				if(compressResults)	//If we are compressing the results for easy graphing...
				{
					if(lineInterval == 1000) //The graph counts in increments of 1000
					{
						if(lineNum < count.size()) 	//If the line number we are on is less than the largest line number counted
							count.set(lineNum,lastCount); //then we set the line number to the count
						else
							count.add(lastCount); //Otherwise, we just add it to the end of the arraylist
						
						lineInterval = 0;
					}
					else
					{
						lineInterval++;
					}
			
				}
			}
			br.close();
		}
		
		sendMessage(channel, "Approx. &BOLD"+linesRead+"&RESET lines analyzed spanning &BOLD"+TimeUnit.MILLISECONDS.toDays(System.currentTimeMillis()-oldestFileTime)+"&RESET days:  &RESET&UNDERLINE"+ub.generateTrendGraph(count)+"&RESET end count was &BOLD"+lastCount);
		
	}

	@Override
	public String[] getAliases()
	{
		
		return new String[]{"trend"};
	}

	@Override
	public String getHelp()
	{
		
		return "<word> [thischannelonly?]- Shows a graph of accumulative word usage over time";
	}

}
