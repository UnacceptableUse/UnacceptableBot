package com.unacceptableuse.ircbot.command;

import com.unacceptableuse.ircbot.UnacceptableBot;

public class CommandGetAccessLevel extends Command
{

	public CommandGetAccessLevel(UnacceptableBot ub)
	{
		super(ub);	
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
	{
		String[] args = message.split(" ");
		
		if(args.length > 1)
			sendMessage(channel, args[1]+"'s access level is "+ub.getAccessLevel(args[1]));
		else
			sendMessage(channel, "Your access level is "+ub.getAccessLevel(sender));
	}

	@Override
	public String[] getAliases()
	{
		return new String[]{"getaccesslevel"};
	}

	@Override
	public String getHelp()
	{
		return " [user] - Gets someones access level, or your own";
	}

}
