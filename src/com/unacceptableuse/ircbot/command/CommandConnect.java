package com.unacceptableuse.ircbot.command;

import com.unacceptableuse.ircbot.UnacceptableBot;

public class CommandConnect extends Command
{

	public CommandConnect(UnacceptableBot ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
	{
		String[] args = message.split(" ");
		if(ub.disabledCommands.contains("channel:"+args[1])){sendMessage(channel, "This is why we can't have nice things");return;}
		sendMessage(channel, "Connected to channel "+args[1]);
		if(args.length <= 2)
			ub.channel(args[1], false);
		else
			ub.channel(args[1], args[2], false);
	}

	@Override
	public String[] getAliases()
	{
		return new String[]{"connect", "join"};
	}

	@Override
	public String getHelp()
	{
		return " <channel> (pass) - Connects to a channel";
	}

}
