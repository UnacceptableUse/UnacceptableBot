package com.unacceptableuse.ircbot.command;

import com.unacceptableuse.ircbot.UnacceptableBot;

public class CommandLocation extends Command
{

	public CommandLocation(UnacceptableBot ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
	{
		if(message.contains("set"))
		{
			String[] args = message.split(" ");
			//System.out.println(sender+" has set their status to "+args[2]+" and will change back on "+args[3]);
			final String alias = ub.findRealName(sender);
			sendMessage(channel, alias.equals("unknown") ? "Name not recognized" : "Location set successfully");
			ub.status.put(alias, message.replace(args[0]+" "+args[1], ""));
			ub.updateTopic();
		}else
		{
			sendMessage(channel, ub.stopic);
		}
	}

	@Override
	public String[] getAliases()
	{
		return new String[]{"location"};
	}

	@Override
	public String getHelp()
	{
		return " [set?] <location> - Sets a location for you ";
	}

}
