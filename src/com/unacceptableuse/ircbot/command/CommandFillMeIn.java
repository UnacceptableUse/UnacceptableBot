package com.unacceptableuse.ircbot.command;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.channels.FileChannel;

import com.unacceptableuse.ircbot.UnacceptableBot;

public class CommandFillMeIn extends Command {

	public CommandFillMeIn(UnacceptableBot ub) {
		super(ub);
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
			throws Exception {

        String[] numOfALines = message.split(" ");
       
		
        if(!channel.startsWith("#") && !ub.disabledCommands.contains("fmichannelchecking"))
        	channel = ub.channels[0];
        
		if(numOfALines.length < 2){sendSyntax(channel);return;}
		
		String numOfLines = numOfALines[1];
		
		if(Integer.parseInt(numOfLines) > 200 && !ub.disabledCommands.contains("limitfillmein")){sendMessage(channel, "That's way too much. Try a smaller number.");return;}
		
		 File file = new File(channel + ".log");
         File file1 = new File(channel + "copy.log");
         UnacceptableBot.copyFileUsingFileChannels(file, file1);
         BufferedReader br = new BufferedReader(new FileReader(file1));
         int countHelp = count(channel + "copy.log");
         int linesToRead = countHelp - Integer.parseInt(numOfLines);
		
		if (linesToRead != 0) {
				int i = 0;
				while (i < linesToRead) {
					br.readLine();
					i++;
				}
				int j = 0;
				while (j < Integer.parseInt(numOfLines)) {
					String line = br.readLine();
					if (line == null) {
						break;
					}
					ub.sendNotice(sender, line);
					j++;
				}

			} else {
				for (int i = 0; i <= countHelp; i++) {
					ub.sendNotice(sender, br.readLine());
				}
			}

			br.close();
	}

	@Override
	public String[] getAliases() {
		return new String[]{"fillmein"};
	}

	@Override
	public String getHelp() {
		return "<num> - the number of PMs you wish to read from the chat log.";
	}
	

	public int count(String filename) throws IOException {
		InputStream is = new BufferedInputStream(new FileInputStream(filename));
		try {
			byte[] c = new byte[1024];
			int count = 0;
			int readChars = 0;
			boolean empty = true;
			while ((readChars = is.read(c)) != -1) {
				empty = false;
				for (int i = 0; i < readChars; ++i) {
					if (c[i] == '\n') {
						++count;
					}
				}
			}
			return (count == 0 && !empty) ? 1 : count;
		} finally {
			is.close();
		}
	}

}
