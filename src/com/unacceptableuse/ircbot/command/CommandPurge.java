package com.unacceptableuse.ircbot.command;

public class CommandPurge extends Command
{

	public CommandPurge(Object ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
			throws Exception
	{
		int before = ub.messagesSent.size()+ub.wordUsage.size()+ub.accessLevels.size();
		
		for(String s : ub.messagesSent.keySet())
			if(ub.messagesSent.get(s) < 2)
				ub.messagesSent.remove(s);
		
		for(String s : ub.wordUsage.keySet())
			if(ub.wordUsage.get(s) < 2)
				ub.wordUsage.remove(s);
		
		for(String s : ub.accessLevels.keySet())
			if(ub.accessLevels.get(s) == 0)
				ub.accessLevels.remove(s);
		
		
		int after = ub.messagesSent.size()+ub.wordUsage.size()+ub.accessLevels.size();
		
		sendMessage(channel, before-after+" entries purged out of "+before+".");
		
		
	}

	@Override
	public String[] getAliases()
	{
		
		return new String[]{"purge"};
	}

	@Override
	public String getHelp()
	{
		
		return "Purges invalid aliases/etc...";
	}
	
	
	@Override
	public int getAccessLevel()
	{
		return 4;
	}

}
