// $codepro.audit.disable commandExecution
package com.unacceptableuse.ircbot.command;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.unacceptableuse.ircbot.UnacceptableBot;

public class CommandSpeedTest extends Command
{
	String downloadSpeed, uploadSpeed;
	
	public CommandSpeedTest(UnacceptableBot ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message) throws Exception
	{
		String[] args = message.toLowerCase().split(" ");
		
		if(args.length < 1)
		{
			sendSyntax(channel);
			return;
		}
		
		String s = null;

        try {
            
        	// run the speedtest command
            // using the Runtime exec method:
            Process p = Runtime.getRuntime().exec("python speedtest_cli.py --share");
            
            BufferedReader stdInput = new BufferedReader(new 
                 InputStreamReader(p.getInputStream()));

            BufferedReader stdError = new BufferedReader(new 
                 InputStreamReader(p.getErrorStream()));

            // read the output from the command
            //System.out.println("Here is the standard output of the command:\n");
        	//System.out.println("---Starting Speedtest---");
        	sendMessage(channel, "---Starting Speedtest---");
            while ((s = stdInput.readLine()) != null)
            {
            	String[] downloadStarting = s.split("Hosted by");
            	String[] uploadStarting = s.split("Download: ");
            	String[] uploadDone = s.split("Upload: ");
            	String[] http = s.split("http://");
            	
            	if(downloadStarting.length != 1)
            	{
            		//System.out.println("Testing download speed...");
            		sendMessage(channel, "Testing download speed...");
            	}
            	
            	if(uploadStarting.length != 1)
            	{
            		//System.out.println("Testing upload speed...");
            		sendMessage(channel, "Testing upload speed...");
            		downloadSpeed = uploadStarting[1];
            		//System.out.println("Download speed: " + downloadSpeed);
            	}
            	
            	if(uploadDone.length != 1)
            	{
            		uploadSpeed = uploadDone[1];
            		//System.out.println("Upload speed: " + uploadSpeed);
            	}
            	
            	if(http.length != 1)
            	{
            		//System.out.println("Image: http://" + http[1].toString());
            		sendMessage(channel, String.format("Download: %s | Upload: %s | Image: http://%s", downloadSpeed, uploadSpeed, http[1].toString()));
            	}               
            }
            
            // read any errors from the attempted command
            System.out.println("Here is the standard error of the command (if any):\n");
            while ((s = stdError.readLine()) != null) {
                System.out.println(s);
            }
        }
        catch (IOException e) {
            System.out.println("exception happened - here's what I know: ");
            e.printStackTrace();
        }
	}

	@Override
	public String[] getAliases()
	{
		return new String[]{"speedtest"};
	}

	@Override
	public String getHelp()
	{
		return "Performs a speedtest";
	}
	
	@Override
	public boolean doesAnnoyTnt64()
	{
		return true;
	}
}
