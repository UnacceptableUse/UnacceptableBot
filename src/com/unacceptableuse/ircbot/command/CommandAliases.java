package com.unacceptableuse.ircbot.command;

public class CommandAliases extends Command
{

	public CommandAliases(Object ub)
	{
		super(ub);
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
	{
		String[] args = message.split(" ");
		if(args.length == 1){sendSyntax(channel); return;}
		
		StringBuilder stb = new StringBuilder();
		
		for(String host : ub.pastNicks.keySet())
		{
			if(ub.pastNicks.get(host).contains(args[1].toLowerCase()))
				for(String user : ub.pastNicks.get(host))
					stb.append(user+" ");			
		}
		
		if(stb.length() > 0)
			sendMessage(channel, args[1]+" has gone by the following aliases: "+stb.toString());
		else
			sendMessage(channel, args[1]+" has not been seen as any other nickname.");
		
		
	}

	@Override
	public String[] getAliases()
	{	
		return new String[]{"alias","aliases","getaliases","nicknames","nicks"};
	}

	@Override
	public String getHelp()
	{	
		return "<username> - Displays usernames this person has had before";
	}
	
	

}
