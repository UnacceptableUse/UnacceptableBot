package com.unacceptableuse.ircbot.command;

public class CommandPringers extends Command
{

	public CommandPringers(Object ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
			throws Exception
	{
		
		sendMessage(channel,ub.disabledCommands.contains("pringersdiss") ? "pringers is a terrible cook 0/10 - gordon ramsay" : "pringers is an excellent cook. 11/10 - gordon ramsay.");
	}

	@Override
	public String[] getAliases()
	{
		
		return new String[]{"cook"};
	}

	@Override
	public String getHelp()
	{
		
		return null;
	}

}
