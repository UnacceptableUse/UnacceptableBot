package com.unacceptableuse.ircbot.command;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.text.NumberFormat;
import java.util.ArrayList;

import com.unacceptableuse.ircbot.UBStatic;
import com.unacceptableuse.ircbot.UnacceptableBot;

public class CommandDebug extends Command
{

	public CommandDebug(Object ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
			throws Exception
	{
		String[] args = message.split(" ");
		
		if(args.length < 2){sendSyntax(channel);return;}
		
		if(args[1].equalsIgnoreCase("blockpolling"))
			sendMessage(channel, "lastBlockTime = "+ub.lastBlockTime+" lastBlockInterval = "+ub.lastBlockInterval);
		else if(args[1].equalsIgnoreCase("save"))
		sendMessage(channel, "saveInterval = "+ub.saveInterval);
		else if(args[1].equalsIgnoreCase("faucet"))
		sendMessage(channel, "dogeBalance = "+ub.dogeBalance);
		else if(args[1].equalsIgnoreCase("staticvars"))
		sendMessage(channel, "BOT_NAME = "+ub.BOT_NAME+" BOT_CHANNEL = "+UnacceptableBot.BOT_CHANNEL+" BOT_SERVER = "+UnacceptableBot.BOT_SERVER);
		else if(args[1].equalsIgnoreCase("relay"))
		sendMessage(channel, "relayChannel = "+ub.relayChannel+" relayTo = "+ub.relayTo);
		else if(args[1].equalsIgnoreCase("booleans"))
		sendMessage(channel, "aloudToMess = "+ub.aloudToMess+" isLive = "+ub.isLive+" wordFound = "+ub.wordFound);
		else if(args[1].equalsIgnoreCase("misc"))
		sendMessage(channel, "boobcount = "+ub.boobcount+" startupTime = "+ub.startupTime+" stopic = "+ub.stopic);
		else if(args[1].equalsIgnoreCase("cleartimeouts"))
			ub.commandProgression.clear();
		else if(args[1].equalsIgnoreCase("channels"))
		{
			StringBuilder stb = new StringBuilder();
			for(String s : ub.getChannels())
				stb.append(s+" ");
			sendMessage(channel, stb.toString());
		}
		else if(args[1].equalsIgnoreCase("memusage"))
		{
			   Runtime runtime = Runtime.getRuntime();
		
			   NumberFormat format = NumberFormat.getInstance();
		
			   StringBuilder sb = new StringBuilder();
			   long maxMemory = runtime.maxMemory();
			   long allocatedMemory = runtime.totalMemory();
			   long freeMemory = runtime.freeMemory();
			    
			   sendMessage(channel, String.format("%s free wam | %s allocated wam | %s max wam | %s dedotated wam", format.format(freeMemory / 1024), format.format(allocatedMemory / 1024), format.format(maxMemory / 1024),  format.format((freeMemory + (maxMemory - allocatedMemory)/1024))));
			    
		}else if(args[1].equalsIgnoreCase("var"))
		{
			  Field f1;
				try
				{
					
					f1 = ub.getClass().getField(args[2]);
					sendMessage(channel, f1.get(ub).toString());
				} catch (NoSuchFieldException e1)
				{
					sendMessage(channel, "No such variable.");
					e1.printStackTrace();
				} catch (SecurityException e1)
				{
					sendMessage(channel, "Variable is private or protected");
					e1.printStackTrace();
				} catch (IllegalArgumentException e)
				{
					sendMessage(channel, "Illegal Argument");
					e.printStackTrace();
				} catch (IllegalAccessException e)
				{
					sendMessage(channel, "Illegal Access");
					e.printStackTrace();
				}         
		}else if(args[1].equalsIgnoreCase("array"))
		{
			  Field f1;
				try
				{
					UBStatic ubs = new UBStatic();
					f1 = ubs.getClass().getField(args[2]);
					String[] arr = (String[])f1.get(ubs);
					StringBuilder stb = new StringBuilder();
					for(String s : arr)
					{
						stb.append(s+", ");
					}
					sendMessage(channel, stb.toString());
				} catch (NoSuchFieldException e1)
				{
					sendMessage(channel, "No such variable.");
					e1.printStackTrace();
				} catch (SecurityException e1)
				{
					sendMessage(channel, "Variable is private or protected");
					e1.printStackTrace();
				} catch (IllegalArgumentException e)
				{
					sendMessage(channel, "Illegal Argument");
					e.printStackTrace();
				} catch (IllegalAccessException e)
				{
					sendMessage(channel, "Illegal Access");
					e.printStackTrace();
				}         
		}
		else if(args[1].equalsIgnoreCase("cleararrays"))
		{
			sendMessage(channel, "&REDCLEARING ALL ARRAYS! "+ub.BOT_NAME.toUpperCase()+" MAY NOT FUNCTION CORRECTLY!");
			sendMessage(channel, "&REDDO NOT ALLOW SAVING IN THIS CONDITION!");
			ub.commandProgression.clear();
			ub.accessLevels.clear();
			ub.defaultStatus.clear();
			ub.disabledCommands.clear();
			ub.messagesSent.clear();
			ub.returnTimes.clear();
			ub.pastNicks.clear();
			ub.timeOnline.clear();
			ub.status.clear();
			ub.wordUsage.clear();
			
			ub.disabledCommands.add("saving");
		}
		else if(args[1].equalsIgnoreCase("reload"))
		{
			ub.disabledCommands.clear();
			ub.commands.clear();
			ub.registerCommands();
			sendMessage(channel, ub.commands.size()+" commands reloaded!");
		}
		else if(args[1].equalsIgnoreCase("reregistercommands"))
		{
			ub.commands.clear();
			ub.registerCommands();
			sendMessage(channel, ub.commands.size()+" commands registered");
		}
		else if(args[1].equalsIgnoreCase("logs"))
		{
			File[] files = new File("./").listFiles();
			ArrayList<File>logs = new ArrayList<File>();
			StringBuilder stb = new StringBuilder();
			for(File f : files)
			{
				if(f.isDirectory() || !f.canRead())continue;
				if(f.getName().startsWith("#") && !f.getName().contains("copy"))
				{
					logs.add(f); stb.append(f.getName()+" ");
				}
				
			}
			sendMessage(channel, logs.size()+" logs: "+stb.toString());
		}
		else if(args[1].equalsIgnoreCase("commands"))
		{
			StringBuilder stb = new StringBuilder();
			for(Command c : ub.commands)
			{
				
				stb.append(c.getAliases()[0]+" ");
			}
			
			sendMessage(channel, stb.substring(413));
		}
		else if(args[1].equalsIgnoreCase("faucetlimit"))
		{
			if(args.length < 3 || ub.getAccessLevel(sender) < 4)return;
			ub.faucetLimit = Integer.parseInt(args[2]);
			sendMessage(channel, "Faucet limit set.");
		}
		else if(args[1].equalsIgnoreCase("soakgoal"))
		{
			if(args.length < 3 || ub.getAccessLevel(sender) < 4)return;
			ub.dogeSoakGoal = Integer.parseInt(args[2]);
			sendMessage(channel, "Soak goal set.");
		}
		else if(args[1].equalsIgnoreCase("updatehelp"))
		{
			sendMessage(channel, "&REDBOT IS NOW OUTPUTTING HELP HTML.");
			File f = new File("../../var/www/html/bot/help.html");
			if(!f.exists())f.createNewFile();
			PrintWriter pw = new PrintWriter(f);
			pw.println("<html><title>UnacceptableBOT Help</title><link type=\"text/css\" rel=\"stylesheet\" href=\"http://fightthetoast.co.uk/assets/bootstrap/css/bootstrap.css\" /><script>"+
			 " var _gaq = _gaq || [];		  _gaq.push(['_setAccount', 'UA-18077037-3v']);			  _gaq.push(['_trackPageview']);  (function() {	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s); })()</script>");
			pw.println("<body style=\"background-color: #F1F2DA\"><b><u>Correct to version "+ub.version+"</b></u><br><table border=1><tr><td>Command</td><td>Description</td><td>Access level</td></tr>");
			for(Command c : ub.commands)
			{
				pw.println("<tr><td>!"+c.getAliases()[0]+"</td><td>"+(c.getHelp() == null ? "No help supplied" : c.getHelp().replace("<","(").replace(">",")").replace("-",""))+"</td><td>"+(c.getAccessLevel() <= 1 ? "ANY" : c.getAccessLevel() <= 2 ? "MOD" : c.getAccessLevel() <= 4 ? "ADMIN" : c.getAccessLevel() >= 1000 ? "SUPER" :  c.getAccessLevel() > 2000 ? "UNACC" : c.getAccessLevel())+"</td></tr>");
			}
			pw.println("</table></body></html>");
			pw.close();
			sendMessage(channel, "&GREENBOT IS DONE!");
		}else	
		{
			sendSyntax(channel);
			return;
		}
	}

	@Override
	public String[] getAliases()
	{	
		return new String[]{"debug"};
	}

	@Override
	public String getHelp()
	{
		
		return "blockpolling/save/staticvars/relay/booleans/misc/memusage/fauctlimit/cleartimeouts/reregistercommands/channels <int>/10";
	}

}
