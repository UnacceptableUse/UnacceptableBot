package com.unacceptableuse.ircbot.command;

import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.unacceptableuse.ircbot.UnacceptableBot;

public class CommandInsult extends Command
{

	public CommandInsult(UnacceptableBot ub)
	{
		super(ub);
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
	{
		String[] args = message.split(" ");
		InputStream is = ub.getUrlContents("http://www.insultme.co/scripts/insult.php");
		try{
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(is);
			doc.getDocumentElement().normalize();
			
			Node nNode = doc.getElementsByTagName("result").item(0);
			sendMessage(channel, args.length > 1 ? nNode.getAttributes().getNamedItem("title").getNodeValue().replace("You're", args[1]+" is") : nNode.getAttributes().getNamedItem("title").getNodeValue());
		
		}catch(Exception e)
		{
			sendMessage(channel, "ERROR: You're a bot crashing "+e.getMessage());
			e.printStackTrace();
		}
	}

	@Override
	public String[] getAliases()
	{
		return new String[]{"insult"};
	}

	@Override
	public String getHelp()
	{
		return " (username) - Insults a person";
	}

}
