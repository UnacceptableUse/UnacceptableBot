package com.unacceptableuse.ircbot.command;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.jibble.pircbot.IrcException;
import org.jibble.pircbot.NickAlreadyInUseException;

import com.unacceptableuse.ircbot.UnacceptableBot;

public class CommandReboot extends Command
{

	public CommandReboot(UnacceptableBot ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
	{
		if(!ub.aloudToMess){sendMessage(channel, "This is why we can't have nice things");return;}
		sendMessage(channel, "Bot rebooting");
		try
		{
			ub.saveThings();
		} catch (UnsupportedEncodingException e2)
		{
			e2.printStackTrace();
		}
		ub.partChannel(channel, "Bot rebooting");
		ub.disconnect();
		//String[] usr = {"#OcelotWorks"};
		//String[] pwd = {"unicornshit"};
		//new UnacceptableBot(BOT_SERVER, usr, pwd, "UnacceptableBOT", 6667);
		try{
			ub.restartApplication();
		}catch(Exception e)
		{
			try
			{
				ub.connect(ub.BOT_SERVER);
			} catch (Exception e1)
			{
				e1.printStackTrace();
			} 
			ub.joinChannel(ub.channels[0], ub.BOT_CHANNEL_PASS);
			sendMessage(sender, "Unable to reboot: "+e.getLocalizedMessage()+" looks like you're stuck with me!");
		}
	}

	@Override
	public String[] getAliases()
	{		
		return new String[]{"reboot"};
	}

	@Override
	public String getHelp()
	{	
		return " - Reboots bot";
	}

}
