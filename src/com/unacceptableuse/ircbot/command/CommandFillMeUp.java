package com.unacceptableuse.ircbot.command;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;

import com.unacceptableuse.ircbot.UnacceptableBot;

public class CommandFillMeUp extends Command
{

	public CommandFillMeUp(Object ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message) throws Exception
	{	
		
        String[] numOfALines = message.split(" ");
       
        if(!channel.startsWith("#") && !ub.disabledCommands.contains("fmichannelchecking"))
        	channel = ub.channels[0];
		
		if(numOfALines.length < 2){sendSyntax(channel);return;}
		
		 File file = new File(channel + ".log");
         File file1 = new File(channel + "copy.log");
         String numOfLines = numOfALines[1];
         UnacceptableBot.copyFileUsingFileChannels(file, file1);
         BufferedReader br = new BufferedReader(new FileReader(file1));
         int countHelp = ub.count(channel + "copy.log");
         int linesToRead = countHelp - Integer.parseInt(numOfLines);

         StringBuilder stb = new StringBuilder();

         if (linesToRead != 0) 
         {
             int i = 0;
             while(i < linesToRead)
             {
             	br.readLine();
             	i++;
             }
             int j = 0;
             while(j < Integer.parseInt(numOfLines))
             {
             	String line = br.readLine();
             	if(line == null)
             	{
             		break;
             	}
             	stb.append(line + "\n");
             	j++;
             }

             try {
             	URL url = new URL("http://pastebin.com/api/api_post.php");
             	
             	URLConnection conn = url.openConnection();
             	
             	conn.setDoOutput(true);
             	
             	OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());

             	writer.write("api_option=paste&api_paste_private=1&api_paste_expire_date=1H&api_dev_key=48def9776be5b916a9c1eb8cd9e41be4&api_paste_code=" 
	                    	+ stb.toString().substring(0, stb.toString().length() - 1));
             	writer.flush();
             	String line;
             	BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
             	while ((line = reader.readLine()) != null) 
             	{
             		ub.sendNotice(sender, "The requested message log has been uploaded for you here and will expire in 1 hour: " + line);
             	}
             	writer.close();
             	reader.close();
             } catch(Exception ex) {
                ex.printStackTrace();
             }
         }
         else 
         {
             for(int i = 0; i <= ub.count(channel + "copy.log"); i++) 
             {
                 sendMessage(sender, br.readLine());
             }
         }
         br.close();
	}

	@Override
	public String[] getAliases()
	{
		return new String[]{"fillmeup"};
	}

	@Override
	public String getHelp()
	{
		return "<x> - Uploads a paste with the last x messages in the log";
	}

}
