package com.unacceptableuse.ircbot.command;

import java.util.HashMap;

import com.unacceptableuse.ircbot.UnacceptableBot;

public class CommandGraph extends Command
{

	public CommandGraph(UnacceptableBot ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
	{
		String[] args = message.split(" ");
		if(args.length < 2)
		{
			sendMessage(channel, "A graph containing message stats can be found here: "+ub.generateStatsGraph(ub.messagesSent));
		}else
		if(args[1].equals("realnames"))
		{
			HashMap<String, Integer> irlMessages = new HashMap<String, Integer>();
			for(String s : ub.messagesSent.keySet())
			{
				String realName = ub.findRealName(s);
				if(irlMessages.get(s) == null)
					irlMessages.put(realName, ub.messagesSent.get(s));
				else
					irlMessages.put(realName, irlMessages.get(s)+ub.messagesSent.get(s));
			}
			sendMessage(channel, "A graph containing message stats can be found here: "+ub.generateStatsGraph(irlMessages));
		}else
		if(args[1].equals("words"))
		{
			if(ub.wordUsage.size() < 1)return;
			sendMessage(channel, "A graph containing message stats can be found here: "+ub.generateStatsGraph(ub.wordUsage));
		}
	
	}

	@Override
	public String[] getAliases()
	{
		
		return new String[]{"graph", "generategraph"};
	}

	@Override
	public String getHelp()
	{
		
		return " [realnames?] - Shows a graph of messages that have been said";
	}
	
	@Override
	public boolean doesAnnoyTnt64()
	{
		return true;
	}

}
