// $codepro.audit.disable commandExecution
package com.unacceptableuse.ircbot.command;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import com.unacceptableuse.ircbot.UnacceptableBot;
import com.unacceptableuse.ircbot.ValueFormatter;

public class CommandUptime extends Command
{

	public CommandUptime(UnacceptableBot ub)
	{
		super(ub);
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message) throws Exception
	{
		
		long millis = UnacceptableBot.getTime() - ub.startupTime;
		
		Process p = Runtime.getRuntime().exec("uptime");
		BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
		
		sendMessage("*status","uptime");
		
		String uptime = br.readLine();
		
		sendMessage(channel, String.format("%s has been up for %s Server has been up for%s, ZNC has been up for%s", ub.BOT_NAME, ValueFormatter.formatTime(millis), uptime.split("up")[1].split("user")[0], ub.ZNCUptime));
		br.close();
	}

	@Override
	public String[] getAliases()
	{
		return new String[]{"uptime"};
	}

	@Override
	public String getHelp()
	{
		
		return "- Displays how long UnacceptableBOT has been up";
	}
}
