package com.unacceptableuse.ircbot.command;

import java.io.UnsupportedEncodingException;

import com.unacceptableuse.ircbot.UnacceptableBot;

public class CommandForceSave extends Command
{

	public CommandForceSave(UnacceptableBot ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
	{
		try
		{
			ub.saveThings();
		} catch (UnsupportedEncodingException e)
		{
			sendMessage(channel, "UnsupportedEncodingException: "+e.getMessage());
			e.printStackTrace();
		}
		sendMessage(channel, "Done!");
		
	}

	@Override
	public String[] getAliases()
	{	
		return new String[]{"forcesave"};
	}

	@Override
	public String getHelp()
	{
		
		return " - Forces "+ub.BOT_NAME+" to save all settings.";
	}

}
