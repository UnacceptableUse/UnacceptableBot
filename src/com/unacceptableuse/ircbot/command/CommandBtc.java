package com.unacceptableuse.ircbot.command;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.Currency;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.unacceptableuse.ircbot.UnacceptableBot;

public class CommandBtc extends Command
{

	public CommandBtc(UnacceptableBot ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message) throws Exception
	{
String[] args = message.split(" ");
		
		boolean showExchanges = false;

		if(args.length > 1){

		args[1] = new BufferedReader(new InputStreamReader(ub.getUrlContents("http://fightthetoast.co.uk/assets/wordtonumber.php?word="+args[1]))).readLine();
		
		if(args.length > 2)
			if(args[2].equalsIgnoreCase("showexchanges"))
				showExchanges = true;		
		}

		try{
		com.google.gson.JsonParser parser = new com.google.gson.JsonParser();
		com.google.gson.JsonObject dogeElement = parser.parse(new InputStreamReader(ub.getUrlContents("https://chain.so/api/v2/get_price/BTC"))).getAsJsonObject();
		JsonArray priceArray = dogeElement.get("data").getAsJsonObject().get("prices").getAsJsonArray();
		
		StringBuilder stb = new StringBuilder();
		
		stb.append((args.length > 1 ? args[1] : "1") + " BTC = ");
		
		for(JsonElement jo : priceArray)
		{
			JsonObject object = jo.getAsJsonObject();
			
			double price = args.length > 1 ? Double.parseDouble(object.get("price").getAsString()) * Double.parseDouble(args[1]) : Double.parseDouble(object.get("price").getAsString());
			DecimalFormat df = new DecimalFormat("#");
			df.setMaximumFractionDigits(8);
			df.setCurrency(Currency.getInstance("USD"));
			if(price > 0)
			stb.append(df.format(price)+" "+object.get("price_base")+(showExchanges ? " "+object.get("exchange")+" | " : " | "));
		}
		

		sendMessage(channel, stb.toString().replace("\"",""));
		}catch(NumberFormatException e)
		{
			sendSyntax(channel);
		}
	}

	@Override
	public String[] getAliases()
	{	
		return new String[]{"btc"};
	}

	@Override
	public String getHelp()
	{
		return " <currencycode> - Displays BTC conversion";
	}

}
