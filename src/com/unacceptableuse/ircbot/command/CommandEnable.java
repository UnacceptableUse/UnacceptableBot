package com.unacceptableuse.ircbot.command;

import com.unacceptableuse.ircbot.UnacceptableBot;

public class CommandEnable extends Command
{

	public CommandEnable(UnacceptableBot ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
	{
		String[] args = message.split(" ");
		
		if(args.length < 2){sendSyntax(channel);return;}
		
		sendMessage(channel, args[1].equalsIgnoreCase("all") ? "All commands re-enabled!" : ub.disabledCommands.contains(args[1]) ? "The command was re-enabled" : "The command is not disabled!");
		
		if(args[1].equalsIgnoreCase("all"))
			ub.disabledCommands.clear();
		else
			ub.disabledCommands.remove(args[1]);
		
		
		
	}

	@Override
	public String[] getAliases()
	{		
		return new String[]{"enable","xyzenable"};
	}

	@Override
	public String getHelp()
	{	
		return " <command> - Re-enables a disabled command";
	}
	
	@Override
	public int getAccessLevel()
	{
		return 4;
	}

}
