package com.unacceptableuse.ircbot.command;

import com.unacceptableuse.ircbot.UnacceptableBot;

public class CommandAdmin extends Command 
{

	public CommandAdmin(UnacceptableBot ub)
	{
		super(ub);
		
	}

	@Override
	public void excecuteCommand(String channel, String sender, String message)
	{
		String[] args = message.split(" ");
		
		if(args.length < 2)
			sendSyntax(channel);
		else
		{
			if(args[1].equals("fuckingstop"))
			{
				ub.status.clear();
				ub.keyword = "UnacceptableUse";
				sendMessage(channel, "&BOLD&REDOK YOU LITTLE SHITS THATS ENOUGH!");
			}else if(args[1].equals("commandlock"))
			{
				ub.aloudToMess = !ub.aloudToMess;
				sendMessage(channel, ub.aloudToMess ? "&BOLDCommands are now &GREENunlocked" : "&BOLDCommands are now &REDlocked");
			}else if(args[1].equals("forcestatusfor"))
			{
				sendMessage(channel, "Changing "+args[2]+"'s status to "+message.replace(args[0]+" "+args[1]+" "+args[2], ""));
				ub.status.put(args[2], message.replace(args[0]+" "+args[1]+" "+args[2], ""));
				ub.updateTopic();
			}else if(args[1].equals("forcestatusupdate"))
			{
				sendMessage(channel, "Forcing...");
				ub.updateTopic();
			}else if(args[1].equals("instance"))
			{
				if(args.length < 3)
					sendMessage(channel, "USAGE: !admin instance server #channel1,#channel2 channel1pass,channel2pass username port pass");
				else
					new UnacceptableBot(args[2], args[3].split(","),args[4].split(","), args[5], Integer.parseInt(args[6]), args[7]);
			}
				
		}
	}

	@Override
	public String[] getAliases()
	{
		return new String[]{"admin"};
	}

	@Override
	public String getHelp()
	{
		return " fuckingstop/commandlock/forcestatusfor/instance - Provides administrative commands";
	}
	
	@Override
	public int getAccessLevel()
	{
		return 1;
	}

}