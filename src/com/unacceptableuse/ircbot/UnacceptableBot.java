package com.unacceptableuse.ircbot;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.NoRouteToHostException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.ConcurrentModificationException;
import java.util.Date;
import java.util.HashMap;
import java.util.ListIterator;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HttpsURLConnection;

import net.pushover.client.PushoverClient;
import net.pushover.client.PushoverException;
import net.pushover.client.PushoverMessage;
import net.pushover.client.PushoverRestClient;

import org.jibble.pircbot.Colors;
import org.jibble.pircbot.IrcException;
import org.jibble.pircbot.NickAlreadyInUseException;
import org.jibble.pircbot.PircBot;
import org.jibble.pircbot.User;
import org.xml.sax.Parser;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.unacceptableuse.ircbot.command.Command;
import com.unacceptableuse.ircbot.command.CommandAdmin;
import com.unacceptableuse.ircbot.command.CommandAlert;
import com.unacceptableuse.ircbot.command.CommandAliases;
import com.unacceptableuse.ircbot.command.CommandAllMessageStats;
import com.unacceptableuse.ircbot.command.CommandBalance;
import com.unacceptableuse.ircbot.command.CommandBoobcount;
import com.unacceptableuse.ircbot.command.CommandBotBeGone;
import com.unacceptableuse.ircbot.command.CommandBroadcast;
import com.unacceptableuse.ircbot.command.CommandBtc;
import com.unacceptableuse.ircbot.command.CommandBug;
import com.unacceptableuse.ircbot.command.CommandBuild;
import com.unacceptableuse.ircbot.command.CommandBurn;
import com.unacceptableuse.ircbot.command.CommandCoin;
import com.unacceptableuse.ircbot.command.CommandCommand;
import com.unacceptableuse.ircbot.command.CommandConnect;
import com.unacceptableuse.ircbot.command.CommandCount;
import com.unacceptableuse.ircbot.command.CommandDebug;
import com.unacceptableuse.ircbot.command.CommandDefaultLocation;
import com.unacceptableuse.ircbot.command.CommandDefine;
import com.unacceptableuse.ircbot.command.CommandDefineUD;
import com.unacceptableuse.ircbot.command.CommandDisable;
import com.unacceptableuse.ircbot.command.CommandDisconnect;
import com.unacceptableuse.ircbot.command.CommandDoge;
import com.unacceptableuse.ircbot.command.CommandDynamic;
import com.unacceptableuse.ircbot.command.CommandEnable;
import com.unacceptableuse.ircbot.command.CommandFaucet;
import com.unacceptableuse.ircbot.command.CommandFillMeIn;
import com.unacceptableuse.ircbot.command.CommandFillMeUp;
import com.unacceptableuse.ircbot.command.CommandForceLoad;
import com.unacceptableuse.ircbot.command.CommandForceSave;
import com.unacceptableuse.ircbot.command.CommandFucksGiven;
import com.unacceptableuse.ircbot.command.CommandFunction;
import com.unacceptableuse.ircbot.command.CommandGetAccessLevel;
import com.unacceptableuse.ircbot.command.CommandGraph;
import com.unacceptableuse.ircbot.command.CommandHashrate;
import com.unacceptableuse.ircbot.command.CommandImage;
import com.unacceptableuse.ircbot.command.CommandInsult;
import com.unacceptableuse.ircbot.command.CommandKHash;
import com.unacceptableuse.ircbot.command.CommandLive;
import com.unacceptableuse.ircbot.command.CommandLocation;
import com.unacceptableuse.ircbot.command.CommandLogstats;
import com.unacceptableuse.ircbot.command.CommandMe;
import com.unacceptableuse.ircbot.command.CommandMessageStats;
import com.unacceptableuse.ircbot.command.CommandMiners;
import com.unacceptableuse.ircbot.command.CommandPing;
import com.unacceptableuse.ircbot.command.CommandPlatn;
import com.unacceptableuse.ircbot.command.CommandPrintout;
import com.unacceptableuse.ircbot.command.CommandQuietTime;
import com.unacceptableuse.ircbot.command.CommandQuote;
import com.unacceptableuse.ircbot.command.CommandRPS;
import com.unacceptableuse.ircbot.command.CommandRand;
import com.unacceptableuse.ircbot.command.CommandRealName;
import com.unacceptableuse.ircbot.command.CommandRemoveCommand;
import com.unacceptableuse.ircbot.command.CommandSentence;
import com.unacceptableuse.ircbot.command.CommandSetAccessLevel;
import com.unacceptableuse.ircbot.command.CommandSoMuchStats;
import com.unacceptableuse.ircbot.command.CommandSoak;
import com.unacceptableuse.ircbot.command.CommandSpeedTest;
import com.unacceptableuse.ircbot.command.CommandStartRelay;
import com.unacceptableuse.ircbot.command.CommandStopRelay;
import com.unacceptableuse.ircbot.command.CommandTime;
import com.unacceptableuse.ircbot.command.CommandTotalSoaked;
import com.unacceptableuse.ircbot.command.CommandTrend;
import com.unacceptableuse.ircbot.command.CommandUb;
import com.unacceptableuse.ircbot.command.CommandUptime;
import com.unacceptableuse.ircbot.command.CommandWolfram;
import com.unacceptableuse.ircbot.command.CommandYoMamma;

public class UnacceptableBot extends PircBot
{
	public String BOT_NAME = "UnacceptableBOT";
	public static final String BOT_PASSWORD = "11114444";
	public static final String BOT_SERVER = "peter.unacceptableuse.com";
	public static final String BOT_CHANNEL = "##OcelotWorks";
	public static final String BOT_CHANNEL_PASS = "unicornshit";
	public static final String BOT_DEBUG_CHANNEL = "##unacceptablebot";
	public String[] channels;
	public String[] passwords;
	
	private HashMap<String, Integer> missedMessages = new HashMap<String, Integer>();
	public HashMap<String, Integer> messagesSent = new HashMap<String, Integer>();
	public HashMap<String, Integer> wordUsage = new HashMap<String, Integer>();
	public HashMap<String, Integer> commandProgression = new HashMap<String, Integer>();	
	public HashMap<String, Integer> accessLevels = new HashMap<String, Integer>();
	public HashMap<String, Long> timeOnline = new HashMap<String, Long>();
	public HashMap<String, Long>returnTimes = new HashMap<String, Long>();
	
	public HashMap<String, String> defaultStatus = new HashMap<String, String>();
	public HashMap<String, String> status = new HashMap<String, String>();
	//public HashMap<String, String> translations = new HashMap<String, String>();
	
	public ArrayList<String> disabledCommands = new ArrayList<String>();
	public CopyOnWriteArrayList<Command> commands = new CopyOnWriteArrayList<Command>();
	public HashMap<String, ArrayList<String>> pastNicks = new HashMap<String, ArrayList<String>>();	
	
	public UnacceptableBot shibe_wanders;
	
	private String hjk = "sex";
	
	
	private HashMap<String, String> relayColours = new HashMap<String, String>();
	
	public MySQLConnection sql;

	public float dogeBalance2 = 0F;
	public int dogeSoakGoal = 100;
	public int playersOnline = 0;
	public int boobcount = 0, saveInterval = 0, lastBlockInterval = 0, faucetLimit = 100, commandcount = 0;
	private int connectTries = 0;
	public long startupTime = 0L, dogeBalance = 0L;
	public String lastBlockTime = "";
	public String stopic = "Shit status is broken";
	public String ZNCUptime = "";

	
	public static final String version = "build 69 rev 69.69 FINAL BETA FFS WOW I should just stop"; //XXX: INCREMENT EVERY TIME YOU BUILD
	
	public final String[][] aliases = 
			{
				{"&CATFACT","UnacceptableUse","UnacceptablePhone", "UUPhone", "UnacceptableUse~", "UnacceptabIeUse","Peter"}, 
				{"jake","boy_wanders[Phone]","boy_wanders[Tablet]", "boy_wanders", "boy_wanders[IG]"}, 
				{"joel", "teknogeek", "teknogeek|LoL|", "teknogeek[mobile]", "teknogeek|IG|", "Jool"}, 
				{"neil", "dvd604","dvd604|2","dvd-604","dvd604-Tablet", "dvd604-Phone"}, 
				{"april", "wolveslogic"},
				{"steve", BOT_NAME}, 
				{"Eki", "Atmalik"},
				{"John", "pringers","pringers_", "Minfa"},
				{"Hilmar", "Swanmark"}
			};
	
//	private String[][] pbKeys = 
//			{
//				{"peter", "i1LUGUwmu3FQ5sxi5MaFWAGipoIkuEAs", "udpyJddxCX"}
//			};
	
	
	private final String[] crashHoly = {"Agility","Almost","Alphabet","Alps","Alter Ego","Anagram","Apparition","Armadillo","Armour Plate","Ashtray","Asp","Astronomy","Astringent Plum-like Fruit","Audubon","Backfire","Ball And Chain","Bank Balance","Bankruptcy","Banks","Bargain Basements","Barracuda","Bat Logic","Bat Trap","Benedict Arnold","Bijou","Bikini","Bill Of Rights","Birthday Cake","Black Beard","Blackout","Blank Cartridge","Blizzard","Bluebeard","Bouncing Boiler Plate","Bowler","Bullseye","Bunions","Caffeine","Camouflage","Captain Nemo","Caruso","Catastrophe","Cats","Chicken Coop","Chilblains","Chocolate Eclair","Cinderella","Cinemascope","Cliche","Cliffhangers","Clockwork","Clockworks","Cofax You Mean","Coffin Nails","Cold Creeps","Complications","Conflagration","Contributing to the Delinquency of Minors","Corpuscles","Cosmos","Costume Party","Crack Up","Crossfire","Crucial Moment","Cryptology","D'artagnan","Detonator","Disappearing Act","Distortion","Diversionary Tactics","Egg Shells","Encore","Epigrams","Escape-hatch","Explosion","Fate-worse-than-death","Felony","Finishing-touches","Fireworks","Firing Squad","Fishbowl","Flight Plan","Flip-flop","Flood Gate","Floor Covering","Flypaper","Fly Trap","Fog","Forecast","Fork In The Road","Fourth Amendment","Fourth Of July","Frankenstein","Frankenstein Its Alive","Fratricide","Frogman","Fruit Salad","Frying Towels","Funny Bone","Gall","Gambles","Gemini","Geography","Ghost Writer","Giveaways","Glow Pot","Golden Gate","Graf Zeppelin","Grammar","Graveyards","Greed","Green Card","Greetings-cards","Guacamole","Guadalcanal","Gullibility","Gunpowder","Haberdashery","Hailstorm","Hairdo","Hallelujah","Halloween","Hallucination","Hamburger","Hamlet","Hamstrings","Happenstance","Hardest Metal In The World","Harem","Harshin","Haziness","Headache","Headline","Heart Failure","Heartbreak","Heidelberg","Helmets","Helplessness","Here We Go Again","Hi-fi","Hieroglyphic","High-wire","Hijack","Hijackers","History","Hoaxes","Hole In A Donut","Hollywood","Holocaust","Homecoming","Homework","Homicide","Hoodwink","Hoof Beats","Hors D'Oeuvre","Horseshoes","Hostage","Hot Foot","Houdini","Human Collectors Item","Human Pearls","Human Pressure Cookers","Human Surfboards","Hunting Horn","Hurricane","Hutzpa","Hydraulics","Hypnotism","Hypodermics","Ice Picks","Ice Skates","Iceberg","Impossibility","Impregnability","Incantation","Inquisition","Interplanetary Yardstick","Interruptions","Iodine","IT and T","Jack In The Box","Jackpot","Jail Break","Jaw Breaker","Jelly Molds","Jet Set","Jigsaw Puzzles","Jitter Bugs","Journey To The Center Of The Earth","Jumble","Karats","Key Hole","Key Ring","Kilowatts","Kindergarten","Knit One Pearl Two","Knock Out Drops","Known Unknown Flying Objects","Kofax","Las Vegas","Leopard","Levitation","Liftoff","Living End","Lodestone","Long John Silver","Looking Glass","Love Birds","Luther Burbank","Madness","Magic Lantern","Magician","Main Springs","Marathon","Mashed Potatoes","Masquerade","Matador","Mechanical Armies","Memory Bank","Merlin Magician","Mermaid","Merry Go Around","Mesmerism","Metronome","Miracles","Miscast","Missing Relatives","Molars","Mole Hill","Mucilage","Multitudes","Murder","Mush","Naive","New Year's Eve","Nick Of Time","Nightmare","Non Sequiturs","Oleo","Olfactory","One Track Bat Computer Mind","Oversight","Oxygen","Paderewski","Paraffin","Perfect Pitch","Pianola","Pin Cushions","Polar Front","Polar Ice Sheet","Polaris","Popcorn","Potluck","Pressure Cooker","Priceless Collection of Etruscan Snoods","Pseudonym","Purple Cannibals","Puzzlers","Rainbow","Rats In A Trap","Ravioli","Razors Edge","Recompense","Red Herring","Red Snapper","Reincarnation","Relief","Remote Control Robot","Reshevsky","Return From Oblivion","Reverse Polarity","Rheostat","Ricochet","Rip Van Winkle","Rising Hemlines","Roadblocks","Robert Louis Stevenson","Rock Garden","Rocking Chair","Romeo And Juliet","Rudder","Safari","Sarcophagus","Sardine","Scalding","Schizophrenia","Sedatives","Self Service","Semantics","Serpentine","Sewer Pipe","Shamrocks","Sherlock Holmes","Show-Ups","Showcase","Shrinkage","Shucks","Skull Tap","Sky Rocket","Slipped Disc","Smoke","Smokes","Smokestack","Snowball","Sonic Booms","Special Delivery","Spider Webs","Split Seconds","Squirrel Cage","Stalactites","Stampede","Standstills","Steam Valve","Stew Pot","Stomach Aches","Stratosphere","Stuffing","Subliminal","Sudden Incapacitation","Sundials","Surprise Party","Switch A Roo","Taj Mahal","Tartars","Taxation","Taxidermy","Tee Shot","Ten Toes","Terminology","Time Bomb","Tintinnabulation","Tipoffs","Titanic","Tome","Toreador","Trampoline","Transistors","Travel Agent","Trickery","Triple Feature","Trolls And Goblins","Tuxedo","Uncanny Photographic Mental Processes","Understatements","Underwritten Metropolis","Unlikelyhood","Unrefillable Prescriptions","Vat","Venezuela","Vertebrae","Voltage","Waste Of Energy","Wayne Manor","Weaponry","Wedding Cake","Wernher von Braun","Whiskers","Wigs","Zorro"};
	
	public String relayChannel = null;
	public String relayTo = null;
	public String keyword = "Unacceptable";
	
	public String[] UDResults, streamResult;
	public boolean wordFound, isLive, aloudToMess = true;
	
	
	public Random rand = new Random();
	
	public UnacceptableBot(String server, String[] channels, String[] passwords, String username, int port, String pass)
	{
		this.channels = channels;
		this.passwords = passwords;
		this.BOT_NAME = username;
		init(server, port,pass);
	}
	
	private void init(String server, int port, String pass)
	{
		dlog("[INIT] Initializing...");
		startupTime = getTime();
		System.setProperty("jsse.enableSNIExtension", "false");
		dlog("[INIT] Starting Command poll thread");
		ThreadPollCommands poll = new ThreadPollCommands(this);
		Thread t = new Thread(poll, "Command Polling Thread");
		t.start();
		setName(BOT_NAME);
		setVerbose(false);
		setVersion("UnacceptableBOT "+version+" by UnacceptableUse");
		setLogin("UnaccB");
		startIdentServer();
		
		//dlog("[INIT] Connecting to MySQL server...");
		 //sql = new MySQLConnection();
//		try
//		{
////			sql.connect();
//		} catch (ClassNotFoundException e1)
//		{
//			dlog("[INIT] ERROR! MySQL Connection failed. JBDC not found. Is the driver installed?");
//			e1.printStackTrace();
//		} catch (SQLException e1)
//		{
//			dlog("[INIT] ERROR! MySQL Connection failed. MySQL error #"+e1.getErrorCode());
//			e1.printStackTrace();
//		}
//		
		
		dlog("[INIT] Connecting to "+BOT_SERVER);
			
		tryConnect(server, port, pass);
		
		dlog("[INIT] Connection successful!");
		connectTries = 0;
		
		dlog("[INIT] Regsitering commands");
		registerCommands();
		dlog("[INIT] "+commands.size()+" commands registered");
		
		
		sendMessage("nickserv", "IDENTIFY UnacceptableBOT " + BOT_PASSWORD);
//
//		try {
//			Thread.sleep(10000);
//		} catch (InterruptedException e1) {
//			e1.printStackTrace();
//		}
		

		joinChannels(); 
		
		dlog("[INIT] Loading settings...");
		try
		{
			loadThings();
		} catch (IOException e) {
			sendMessage(channels[0], "Failed to load shit. wtf did you do");
			e.printStackTrace();
		}
	}
	
	
	public HashMap<String, Integer> generateWordArray(String channel)
	{
		File logs = new File(channel+".log");	

		try{
			BufferedReader br = new BufferedReader(new FileReader(logs));
			
			HashMap<String, Integer> wordUsage = new HashMap<String, Integer>();
	
				String line = "";
				while((line = br.readLine()) != null)
				{
					line = line.replaceAll("(\\[(.*?)\\]|\\<(.*?)\\>)", "").replaceAll("([^a-zA-Z ]+)",""); // From this point on all operations use the message and not the sender or time/date or any punctuation
					
					String[] words = line.split(" ");		
					for(String s : words)
					{
						s = s.toLowerCase();
						
						if(s.length() > 0) //Gets rid of all blank words that my have slipped through
							wordUsage.put(s, wordUsage.containsKey(s) ? wordUsage.get(s)+1 : 1);
					}
					
				}
				
				br.close(); 
		}catch(Exception e)
		{
			return null;
		}
	        
	    return wordUsage;
	}
	
	
	public void addCommand(Command c)
	{
		commands.add(c);
	}
	
	
	public void dlog(String message)
	{
		sendMessage(BOT_DEBUG_CHANNEL, message);
		System.out.println("["+new Date().toString()+"] "+message);
	}

	
	private void tryConnect(String server, int port, String pass)
	{
		connectTries++;
		if(connectTries > 20)
		{
			System.err.println("[INIT] Tried to connect too many times.");
		
			this.disconnect();	
			this.dispose();	
		}
		try{
			connect(server, port, pass);
		}catch(NickAlreadyInUseException e)
		{
			System.err.println("[INIT] Nickname already in use, attemting to use secondary username");
			BOT_NAME = BOT_NAME+"_";
			setName(BOT_NAME);
			this.disconnect();
			tryConnect(server, port, pass);
		}catch(IOException e1)
		{
			System.err.println("[INIT] IOException: "+e1.getMessage()+". Trying again in "+connectTries+" seconds.");
		
			while(this.isConnected())
			{
				System.err.println("[INIT] Still connected?");
				this.disconnect();
				try{Thread.sleep(1000 * connectTries);} catch (InterruptedException e){e.printStackTrace();}
			}
			
			

			tryConnect(server, port, pass);
		}catch(IrcException e2)
		{
			System.err.println("[INIT] IRCException");
			e2.printStackTrace();
		}
	}
	
//	@Beta
//	public String getTranslationFor(String key)
//	{
//		return translations.get(key);
//	}
	
	@Override
	public void onVersion(String sourceNick,String sourceLogin,String sourceHostname,String target)
	{
		sendNotice(sourceNick, BOT_NAME+" "+version+" by UnacceptableUse.");
	
	}

	public void pollBlocks()
	{
		if(disabledCommands.contains("blockpolling"))return;
		lastBlockInterval++;
		if(lastBlockInterval > 60)
		{
			lastBlockInterval = 0;
			JsonParser json = new JsonParser();
			JsonArray coinArray = json.parse(new InputStreamReader(getHTTPSUrlContents("https://coinking.io/api.php?key=c639476cfa3a8ab1bdf8a1532d7c5ddb&type=last50blocks&output=json"))).getAsJsonArray();
			JsonObject firstBlock = coinArray.get(0).getAsJsonObject();
			if(!firstBlock.get("time").getAsString().equals(lastBlockTime))
			{
				lastBlockTime = firstBlock.get("time").getAsString();
				sendMessage(channels[0], String.format("&GREEN&BOLD%s&reset block found by &BOLD%s&reset | Height: &BOLD%s&RESET| Difficulty: &BOLD%s&RESET | Reward: &BOLD%s&RESET | Confirmations: &BOLD%s&RESET | Found at &BOLD%s&RESET",
						firstBlock.get("coin").toString().toUpperCase(),
						firstBlock.get("finder").toString(),
						firstBlock.get("height").toString(),
						firstBlock.get("difficulty").toString(),
						firstBlock.get("coins").toString(),
						firstBlock.get("confirmations").toString(),
						firstBlock.get("time").toString()).replace("\"", ""));
			}
		}
	}
	
	public void registerCommands()
	{		
		dlog("[COMMANDS] Registering commands..");
		
		//commands.add(new CommandPurge(this));      	=PARTICALLY OBSELETE=
		commands.add(new CommandBurn(this));
		//commands.add(new CommandSQLTest(this)); 		=OBSOLETE=
		commands.add(new CommandWolfram(this));
		commands.add(new CommandLogstats(this));
		commands.add(new CommandAllMessageStats(this));
		commands.add(new CommandMessageStats(this));
		commands.add(new CommandSoMuchStats(this));
		commands.add(new CommandGraph(this));
		commands.add(new CommandBoobcount(this));// 	=PARTIALLY OBSELETE=	
		commands.add(new CommandCount(this));
		commands.add(new CommandEnable(this));
		//commands.add(new CommandTimeOnline(this)); 	=BROKEN=
		commands.add(new CommandBalance(this));
		commands.add(new CommandMiners(this));
		commands.add(new CommandCoin(this));
		commands.add(new CommandHashrate(this));
		//commands.add(new CommandLastBlock(this)); 	=FUCKING MRBOTTERSON WITH HIS SUPIRIOR COMMANDS=
		//commands.add(new CommandDifficulty(this)); 	=BROKEN=
		commands.add(new CommandKHash(this));
		commands.add(new CommandBtc(this));
		commands.add(new CommandDoge(this));
		commands.add(new CommandMe(this));
		commands.add(new CommandConnect(this));
		//commands.add(new CommandSilentConnect(this)); =OBSOLETE=
		commands.add(new CommandDisconnect(this));
		commands.add(new CommandBotBeGone(this));
		//commands.add(new CommandShutdown(this)); 		=OBSOLETE=
		//commands.add(new CommandReboot(this)); 		=BROKEN=
		commands.add(new CommandForceSave(this));
		commands.add(new CommandForceLoad(this));
		commands.add(new CommandAdmin(this));
		commands.add(new CommandGetAccessLevel(this));
		commands.add(new CommandSetAccessLevel(this));
		commands.add(new CommandDisable(this));
		commands.add(new CommandPrintout(this));
		commands.add(new CommandAliases(this));
		commands.add(new CommandStartRelay(this));
		commands.add(new CommandStopRelay(this));
		commands.add(new CommandDefine(this));
		commands.add(new CommandDefineUD(this));
		commands.add(new CommandTime(this));
		commands.add(new CommandLive(this));
		commands.add(new CommandImage(this));
		commands.add(new CommandInsult(this));
		commands.add(new CommandLocation(this));
		commands.add(new CommandBuild(this));
		commands.add(new CommandPing(this));
		commands.add(new CommandAlert(this));
		commands.add(new CommandRand(this));
		commands.add(new CommandRealName(this));
		commands.add(new CommandFillMeIn(this));
		commands.add(new CommandFillMeUp(this));
		commands.add(new CommandUb(this));
		commands.add(new CommandUptime(this));
		commands.add(new CommandSpeedTest(this));
		commands.add(new CommandDebug(this));
		commands.add(new CommandCommand(this));
		commands.add(new CommandFaucet(this));
		commands.add(new CommandDefaultLocation(this));
		commands.add(new CommandYoMamma(this));
		commands.add(new CommandSentence(this));
		commands.add(new CommandQuote(this));
		commands.add(new CommandPlatn(this));
		commands.add(new CommandFucksGiven(this));
		commands.add(new CommandBug(this));
		commands.add(new CommandBroadcast(this));
		commands.add(new CommandFunction(this));
		commands.add(new CommandRemoveCommand(this));
		commands.add(new CommandRPS(this));
		commands.add(new CommandTrend(this));
		commands.add(new CommandQuietTime(this));
		commands.add(new CommandSoak(this));
		commands.add(new CommandTotalSoaked(this));
		
		commands.add(new CommandDynamic(this,"rekt","☑ rekt ☐ not rekt  http://i.imgur.com/x7Yb626.gif"));
		commands.add(new CommandDynamic(this,"huehue","http://huehuehuehue.com"));
		commands.add(new CommandDynamic(this,"unacceptable","http://unacceptableuse.com"));
		commands.add(new CommandDynamic(this,"pringers","Pringers is choose:awesome:epic:well-endowed:great:amazing:sexy:sweet:OP:manly"));
		commands.add(new CommandDynamic(this,"tikiana","Tikiana choose:smells:tastes:feels like choose:roses:lemons:semen:seaside:pringers"));
		commands.add(new CommandDynamic(this,"cook","Pringers is an excellent cook. 11/10 - gordon ramsay."));
		commands.add(new CommandDynamic(this,"roulette","choose:&BOLDBANG!&RESET,_you_lose!:Chamber_not_loaded:Chamber_not_loaded:Chamber_not_loaded:Chamber_not_loaded:Chamber_not_loaded:Chamber_not_loaded"));
		commands.add(new CommandDynamic(this,"ohfuk","Me n you are like eachother if you think about it, cause cum comes out of our penises and we watch porn (I was watchin porn when it came out... I was lookin at how to make your dick bigger) :)"));
		
		
		
		disabledCommands.add("console:grep");
		disabledCommands.add("console:ping");
		disabledCommands.add("console:rm");
		disabledCommands.add("console:ls");
		disabledCommands.add("console:cd");
		disabledCommands.add("console:sudo");
		disabledCommands.add("console:psswd");
		
		disabledCommands.add("mining");
		disabledCommands.add("blockpolling");
		disabledCommands.add("boobs");
		disabledCommands.add("tnt64limits");
		disabledCommands.add("pridemode");
		disabledCommands.add("verbose");
		disabledCommands.add("adfghjkl");
		disabledCommands.add("consoleDisableAt:10");
		this.commandProgression.put("player:logging", 10);
		
		
	}
	
//	public ArrayList<Command> loadCommands() throws Exception
//	{
//		File myFile = new File("commands.jar");
//		URL myJarFileURL = new URL("jar", "", "file:" + myFile.getAbsolutePath() + "!/");
//		URL[] classes = {myJarFileURL};
//		URLClassLoader child = new URLClassLoader (classes, this.getClass().getClassLoader());
//        Class classToLoad = Class.forName ("com.unacceptableuse.ircbot.command.CommandLoader", true, child);
//        Method method = classToLoad.getDeclaredMethod ("getCommands");
//        CommandLoader instance = (CommandLoader) classToLoad.newInstance();
//        instance.getCommands(this);
//        
//        
//       // Object result = method.invoke(instance);
//        
//        return (ArrayList<Command>) result;
//	}
	
	
	public void joinChannels()
	{
		dlog("[JOIN] Joining Channels...");
		for(int i = 0; i < channels.length; i++)
		{
			if(passwords[i] == null)
				channel(channels[i], true);
			else
				channel(channels[i], passwords[i], false);
		}
	}
	
	
	@Override
	public void onNickChange(String oldNick,String login,String hostname,String newNick)
	{
		dlog("[NICKCHANGE] "+oldNick+" changed to "+newNick+".");
		if(pastNicks.get(login) == null)
		{
			ArrayList<String> arr = new ArrayList<String>();
			arr.add(oldNick.toLowerCase());
			arr.add(newNick.toLowerCase());
			pastNicks.put(login, arr);
			dlog("[NICKCHANGE] "+oldNick+" and "+newNick+" have been added to the database.");
		}else
		{
			dlog("[NICKCHANGE] Nicknames for "+login+" have been updated.");
			ArrayList<String> arr = pastNicks.get(login);
			if(!arr.contains(newNick.toLowerCase()))
				arr.add(newNick.toLowerCase());
			if(!arr.contains(oldNick.toLowerCase()))
				arr.add(oldNick.toLowerCase());
		}

	}
	
	
	@Override
	public void onMessage(String channel, String sender, String login, String hostname, String message)
	{

			saveInterval++;
				
			if(sender.equals("***"))return; //Don't process ZNC messages
			
			if(sender.equals("[MC]-DogeFest") && message.contains("<"))
			{
					sender = message.replace("[MineCraft] <", "").split(">")[0];
					message = message.substring(message.indexOf(">")+2);
			}
			
			if(sender.equals("WanderBOT"))
			{
				sender = Colors.removeFormattingAndColors(message.replace("(", "").split(")")[0]).replace("~","");
				message = message.substring(message.indexOf(")")+2);
			}
			
			if(disabledCommands.contains("ppadv") && channel.equals("#doge-coin") && (message.toLowerCase().contains("hot sauce") || message.toLowerCase().contains("peppers") || message.toLowerCase().contains("scorpion") || message.toLowerCase().contains("chillies")))
			{
				sendMessage(channel, "Visit pexpeppers.com");
			}
			
			
			if(sender.equals("DogeWallet"))
			{
				if(message.contains("UnacceptableBOT"))
				{
					sendMessage("DogeWallet", ".balance");
				}
			}
				
			
			if(!disabledCommands.contains("adfghjkl"))
				if(sender.contains("tekno"))
				{
					if(message.contains("sex") || message.contains("!image nsfw"))
						kick(channel, sender, "Bad joel!");
					
					if(message.equalsIgnoreCase(hjk))
						kick(channel, sender, "No spamming joel");
					
					hjk = message;
				}
		
						
				
			
			if(message.equalsIgnoreCase("test"))
				sendMessage(channel,"icles");
		
			try
			{
				if(saveInterval > 25 && !disabledCommands.contains("saving"))
				{
					//dlog("["+channel.toUpperCase()+"] Saving...");
					saveInterval = 0;
					saveThings();
				}

				updateAutoStatus();
		
				if((message.toLowerCase().contains("boob") || message.contains("tit")) && !message.startsWith("!"))
					boobcount++;
				if(relayChannel != null && !disabledCommands.contains("relaying"))
				{	
					if(!relayColours.containsKey(sender))
					{
						String newKey = Colors.BLUE;
						int randInt = rand.nextInt(16);
						switch(randInt)
						{
						case 0:
							newKey = Colors.BLACK;
							break;
						case 1:
							newKey = Colors.BLUE;
							break;
						case 2:
							newKey = Colors.BROWN;
							break;
						case 3:
							newKey = Colors.CYAN;
							break;
						case 4:
							newKey = Colors.CYAN;
							break;
						case 5:
							newKey = Colors.DARK_BLUE;
							break;
						case 6:
							newKey = Colors.DARK_GRAY;
							break;
						case 7:
							newKey = Colors.DARK_GREEN;
							break;
						case 8:
							newKey = Colors.GREEN;
							break;
						case 9:
							newKey = Colors.LIGHT_GRAY;
							break;
						case 10:
							newKey = Colors.MAGENTA;
							break;
						case 11:
							newKey = Colors.NORMAL;
							break;
						case 12:
							newKey = Colors.OLIVE;
							break;
						case 13:
							newKey = Colors.PURPLE;
							break;
						case 14:
							newKey = Colors.RED;
							break;
						case 15:
							newKey = Colors.TEAL;
							break;
						case 16:
							newKey = Colors.YELLOW;
							break;
							
						}
						relayColours.put(sender, newKey);	
					}
									
					if(channel.toLowerCase().equals(relayChannel.toLowerCase()) || relayChannel.equalsIgnoreCase("all"))
						sendMessage(relayTo,"<"+Colors.BOLD+relayColours.get(sender)+sender+Colors.NORMAL+"> "+message);				
				}
				
//				if(messagesSent.get(sender) != null)
//					messagesSent.put(sender, messagesSent.get(sender)+1);
//				else
//					messagesSent.put(sender, 1);	
				
				
				if(sender.equals(BOT_NAME) && !disabledCommands.contains("sanitycheck"))return;
				
				
				//message = message.toLowerCase();

				
				
				ListIterator<Command> it = commands.listIterator();    
		        while(it.hasNext() && !disabledCommands.contains("commands")) 
				{
		        	Command c = it.next();
					if(c.getAliases() == null )
					{
						sendMessage(BOT_DEBUG_CHANNEL, "Command "+c+" has no aliases set!");
						//break; sdsd
					}
					for(String s : c.getAliases())
					{		
						if(message.toLowerCase().startsWith("!"+s) && !disabledCommands.contains(s))
						{	
							if(message.length() > s.length()+1)
								if(message.charAt(s.length()+1) != " ".charAt(0))return;
							
							if(getAccessLevel(sender) >= c.getAccessLevel())
							{
								if(disabledCommands.contains("catfacts:"+sender.toLowerCase()))
								{
									sendMessage(channel, "Thank you for signing up for Cat Facts!");
									sendMessage(channel, generateRandomCatFact());
									return;
								}
								
								if(channel.contains("votedogecar") && !disabledCommands.contains("tnt64limits"))
								{
									if(!commandProgression.containsKey(sender+":Cooldown:"+s) && !disabledCommands.contains("annoyingcommands") || getAccessLevel(sender) > 3)
									{
										dlog("["+channel.toUpperCase()+"] "+sender+" performed "+s+" ("+message+")");
										commandProgression.put(sender+":Cooldown:"+s, 10);
										c.excecuteCommand(channel, sender, message);
									}else
										sendNotice(sender, "You cannot use this command for another "+commandProgression.get(sender+":Cooldown:"+s)+" seconds!");		
								}else
								{
									dlog("["+channel.toUpperCase()+"] "+sender+" performed "+s+" ("+message+")");
									c.excecuteCommand(channel, sender, message);
								}
								updateCommandCount();
								break;
							}else
								if(getAccessLevel(sender) > -2)
								sendMessage(channel,getAccessLevel(sender) == -1 ? Colors.RED+"I will not serve you, "+sender : "You do not have permission to perform this command!");
						}	
					}
				}
				
				String pattern = "(?<=watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";

			    Pattern compiledPattern = Pattern.compile(pattern);
			    Matcher matcher = compiledPattern.matcher(message);

			    if(matcher.find() && !disabledCommands.contains("youtube") && getAccessLevel(sender) > 0){
			    	try{
					  InputStream is = getHTTPSUrlContents("https://gdata.youtube.com/feeds/api/videos/"+matcher.group().replace("","").replace(",","").replace(".","").split(" ")[0]+"?v=2&alt=json");
					  com.google.gson.JsonParser parser = new com.google.gson.JsonParser();
					  
					  String title = parser.parse(new InputStreamReader(is)).getAsJsonObject().get("entry").getAsJsonObject().get("title").getAsJsonObject().get("$t").getAsString();
					  sendMessage(channel, Colors.BOLD+"Youtube link: "+title);
					  is.close();
			    	}catch(Exception e)
			    	{
			    		e.printStackTrace();
			    	}
			    }
			    
			    try{
			    //TODO: fancy regex for this
			    if(message.contains("/r/") && !message.contains("reddit.com") && !disabledCommands.contains("subredditparsing") && getAccessLevel(sender) > 0)
			    {
			    	String subreddit = message.split("/r/")[1].split(" ")[0];
			    	InputStream is = getUrlContents("http://api.reddit.com/r/"+subreddit.replace(",","").replace(".","")+"/about");
				      com.google.gson.JsonParser parser = new com.google.gson.JsonParser();
			    	
			    	String subredditDesc = parser.parse(new InputStreamReader(is)).getAsJsonObject().get("data").getAsJsonObject().get("public_description").getAsString();
			    	sendMessage(channel, Colors.BOLD+"http://reddit.com/r/"+subreddit+" - "+subredditDesc);
			    }
			    
			    if(message.contains("/u/") && !message.contains("reddit.com") && !disabledCommands.contains("userparsing") && getAccessLevel(sender) > 0)
			    {
			    	String user = message.split("/u/")[1].split(" ")[0];
			    	InputStream is = getUrlContents("http://api.reddit.com/u/"+user.replace(",","").replace(".","")+"/about");
				      com.google.gson.JsonParser parser = new com.google.gson.JsonParser();
			    	
				    JsonObject jo = parser.parse(new InputStreamReader(is)).getAsJsonObject().get("data").getAsJsonObject();
				      
			    	int linkKarma = jo.get("link_karma").getAsInt();
			    	int commentKarma = jo.get("comment_karma").getAsInt();
			    	sendMessage(channel, Colors.NORMAL+Colors.BOLD+"http://reddit.com/u/"+user+" - "+linkKarma+" Link Karma. "+commentKarma+" Comment Karma.");
			    }
			    }catch(Exception e)
			    {}
			    
			    if(message.contains("big ass ") && !disabledCommands.contains("bigass"))
			    	sendMessage(channel, Colors.BOLD+"ass-"+message.split("big ass ")[1].split(" ")[0]);
			    	
			    
			    if(!disabledCommands.contains("logging")){
					try {
						PrintWriter out = new PrintWriter(new FileWriter(channel + ".log",
								true));
						out.write("[" + new java.util.Date() + "] <" + sender + "> "
								+ message + "\n");
						
						out.close();
					} catch (IOException e) {
						sendMessage(channel, "Something just fucked up....");
						e.printStackTrace();
					}
			    }		
			}catch(ArrayIndexOutOfBoundsException e)
			{
				sendMessage(channel, "The command requires arugments but has no failsafe set. ("+e.getMessage()+")");
				logException(sender+" performed "+message, e);
			}catch(NullPointerException e)
			{
				sendMessage(channel, "The command encountered a variable with no value.");
				logException(sender+" performed "+message, e);
			}catch(ArithmeticException e)
			{
				sendMessage(channel, "The command attempted to perform an impossible mathmatical problem. ("+e.getMessage()+")");
				logException(sender+" performed "+message, e);
			}catch(FileNotFoundException e)
			{
				sendMessage(channel, "The command attempted to read from a file that does not exist. ("+e.getMessage()+")");
				logException(sender+" performed "+message, e);
			}catch(ConcurrentModificationException e)
			{
				sendMessage(channel, "The command attempted to write to an array that was busy. ("+e.getMessage()+")");
				logException(sender+" performed "+message, e);
				e.printStackTrace();
			}catch(NumberFormatException e)
			{
				sendMessage(channel, "The command attempted to use a number that was impossible. ("+e.getMessage()+")");
				logException(sender+" performed "+message, e);
			}
			catch(Exception e)
			{
				sendMessage(channel, "Holy "+crashHoly[rand.nextInt(crashHoly.length)]+" Batman! An unexpected error occurred: "+e.getMessage()+" Please use !bug <text> to submit a bug report");
				logException("[UNHANDLED EXCEPTION!] "+sender+" performed "+message, e);
				e.printStackTrace();
			}
	}
	
	public static boolean arrayContains(String[] array, String tofind)
	{
		for(String s : array)
			if(s.contains(tofind))
				return true;
		return false;
	}
	
	public void updateCommandCount()
	{
		commandcount++;
		if(getNick().equals("UnacceptableBOT"))
			setTopic(BOT_DEBUG_CHANNEL, commandcount+" (UnacceptableBOT) + "+shibe_wanders.commandcount+" (Shibe_Wanders) commands used this session.");
	}
	
	
	
	
	public String generateRandomCatFact()
	{
		
		JsonParser p = new JsonParser();
		JsonObject factObject = p.parse(new InputStreamReader(getUrlContents("http://catfacts-api.appspot.com/api/facts"))).getAsJsonObject();
		String fact = factObject.get("facts").getAsJsonArray().get(0).getAsString();
		return fact+" "+ValueFormatter.chooseFromArray(UBStatic.catFactsPrefixes);
	}

	public static void logException(String desc, Exception e)
	{
		try
		{
			FileWriter fw = new FileWriter("errors.txt", true);
			fw.write("["+new Date().toString()+"] "+desc+": "+e.getClass().getName()+" ("+e.getMessage()+") - "+e.getStackTrace()[0]+"\n");		
			fw.close();
			
		}catch (FileNotFoundException e1)
		{
			e1.printStackTrace();
		} catch (IOException e1)
		{	
			e1.printStackTrace();
		}
	}
	
	public String generateTrendGraph(ArrayList<Integer> values)
	{
		StringBuilder data = new StringBuilder();
    	for(int i = 0; i < values.size(); i++)
    	{
    		data.append(i+"="+values.get(i));
    		if(i < values.size()-1)
    		data.append('&');
    	}
    	
    	return generateGraph(data.toString()).replace("graph","graph2");
	}
	
	public String generateStatsGraph(Map<String, Integer> values)
	{
		
    	StringBuilder data = new StringBuilder();
    	int i = 0;
    	for(String s : values.keySet())
    	{
    		i++;
    		data.append(s+"="+values.get(s));
    		if(i < values.size()-1)
    		data.append('&');
    	}
    	
    	return generateGraph(data.toString());
	}
	
	
	public String generateGraph(String content)
	{
		if(disabledCommands.contains("graphs"))return "Graphs disabled";
		
        try {
        	URL url = new URL("http://"+UBStatic.SITE_ROOT+"/generateGraph.php");
        	HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        	conn.addRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)");
        	conn.setDoOutput(true);
        	conn.setRequestMethod("POST");
        	
        	OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
        	
        	
        	writer.write(content);
        	writer.flush();
        	String line;
        	BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        	while ((line = reader.readLine()) != null) 
        	{
        		return line;
        	}
        	writer.close();
        	reader.close();
        } catch(NoRouteToHostException e){
        	return "unacceptableuse.com is down...";
        } catch(Exception ex) {
        	 ex.printStackTrace();
 			logException("[UNHANDLED EXCEPTION] Generating graph", ex);
        	return "An error occured: "+ex.getMessage();
        }
        return null;
	}
	

	public void pollCommands()
	{
		
		
		if(!commandProgression.isEmpty() && !disabledCommands.contains("commandpolling"))
		{
			for(String s : commandProgression.keySet())
			{
				if(s.split(":")[1].equals("help"))
				{
					commands.get(commandProgression.get(s)).sendSyntax(s.split(":")[0]);
					commandProgression.put(s, commandProgression.get(s)+1);
					if(commandProgression.get(s) > commands.size()-1)
					{
						commandProgression.remove(s);
						dlog("[DEBUG] "+s+" has finished receiving the help");
					}					
				}else
				if(s.split(":")[1].equals("DogeTimeout"))
				{
					commandProgression.put(s, commandProgression.get(s)-1);
					if(commandProgression.get(s) < 0)
					{
						commandProgression.remove(s);
						dlog("[DEBUG] "+s+" can now receive dogecoin again.");
					
					}	
				}else
				if(s.split(":")[1].equals("Timeout"))
				{
					commandProgression.put(s, commandProgression.get(s)-1);
					if(commandProgression.get(s) < 0)
					{
						commandProgression.remove(s);
						dlog("[DEBUG] "+s+" can now receive perform "+s.split(":")[2]+" again.");
					
					}	
				}else
				if(s.contains("player:logging"))
				{
					commandProgression.put(s, commandProgression.get(s)-1);
					if(commandProgression.get(s) < 0)
					{
						commandProgression.put(s, 1200);
						InputStream is = getUrlContents("http://mcapi.sweetcode.de/api/v2/?info&ip=dogefest.coffeenet.org");
						
						JsonParser parser = new JsonParser();
						JsonObject jo = parser.parse(new InputStreamReader(is)).getAsJsonObject();
						
						playersOnline = jo.get("player").getAsJsonObject().get("online").getAsInt();
						
						dlog("[DEBUG] dogefest has "+playersOnline+" online.");
						try {
							PrintWriter out = new PrintWriter(new FileWriter("dogefestplayers.log",
									true));
							out.write("[" + new java.util.Date() + "] Players online: "+playersOnline+"\n");
							
							out.close();
						} catch (IOException e) {
							sendMessage("#dogefest", "Something just fucked up (Saving player's online.) "+e.getMessage());
							e.printStackTrace();
						}
					
					}	
				}
			}
		}
	}
	

	
	public void pushoverAlert(String userID, String sender, String channel) throws PushoverException
	{
		PushoverClient client = new PushoverRestClient();        

		client.pushMessage(PushoverMessage.builderWithApiToken("agWCerCUmTawsK4Nrf6a1JebgNVqvo")
				.setUserId(userID)
				.setMessage(sender + " has alerted you on " + channel + ".")
				.build());
	}
	
//	private String getPbKey(String sender)
//	{
//		final String realName = findRealName(sender);
//		for(String[] s : pbKeys)
//		{
//			if(s[0].equals(realName))
//				return s[1];
//		}
//		return null;
//	}
//	
//	private String getPbIdent(String sender)
//	{
//		final String realName = findRealName(sender);
//		for(String[] s : pbKeys)
//		{
//			if(s[0].equals(realName))
//				return s[2];
//		}
////		return null;
////	}
//	
//	private SSLSocketFactory getSSL()
//	{
//		return (SSLSocketFactory) SSLSocketFactory.getDefault();
//	}
//	
//	private void pushAlert(String sender) throws Exception
//	{
//		sendMessage(BOT_CHANNEL, "Attempting to push...");
//	    final SSLSocketFactory sslSocketFactory = getSSL();
//
//	    final HttpsURLConnection connection = (HttpsURLConnection) new URL(
//	            "https://api.pushbullet.com/api/pushes").openConnection();
//	    connection.setSSLSocketFactory(sslSocketFactory);
//
//
//	    connection.setRequestMethod("POST");
//
//	    connection.setUseCaches(false);
//	    connection.setDoInput(true);
//	    connection.setDoOutput(true);
//
//	    final String authStr =  getPbKey(sender)+":";
//	    final String authEncoded = Base64.encodeBase64String(authStr.getBytes());
//	    connection.setRequestProperty("Authorization", "Basic " + authEncoded);
//	    
//	    OutputStreamWriter osw = new OutputStreamWriter(connection.getOutputStream());
//	    osw.write("device_iden="+getPbIdent(sender)+"&type=note&title=IRC+Alert&body="+sender+"+requires+your+attention!");
//
//	    osw.close();
//	    
//	    
//	    final InputStream is = connection.getInputStream();
//	    final BufferedReader rd = new BufferedReader(new InputStreamReader(is));
//	    String line;
//	    final StringBuffer response = new StringBuffer();
//	    while ((line = rd.readLine()) != null) {
//	        response.append(line);
//	        response.append('\r');
//
//
//	    }
//	    rd.close();
//
//	    connection.disconnect();
//	    sendMessage(BOT_CHANNEL, response.toString());
//	}

	  	  
	  
//	public String uploadPaste(String content)
//	{
//        try {
//        	URL url = new URL("http://fightthetoast.co.uk/ub/index.php/api/create?apikey=unacceptablebot");
//        	HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//        	conn.setDoOutput(true);
//        	conn.setRequestMethod("POST");
//        	
//        	OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
//
//        	writer.write("text=" + content.replace(" ", "+")+"&title=UnacceptableBOT+paste&name=UnacceptableBOT&lang=properties&expire=60");
//        	writer.flush();
//        	String line;
//        	BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
//        	while ((line = reader.readLine()) != null) 
//        	{
//        		return line;
//        	}
//        	writer.close();
//        	reader.close();
//        } catch(Exception ex) {
//        	 ex.printStackTrace();
//        	return "An error occured: "+ex.getMessage();
//        }
//        return null;
//	}
	  
	
	 
      public static void copyFileUsingFileChannels(File source, File dest)
                      throws IOException {
              FileChannel inputChannel = null;
              FileChannel outputChannel = null;
              try {
                      inputChannel = new FileInputStream(source).getChannel();
                      outputChannel = new FileOutputStream(dest).getChannel();
                      outputChannel.transferFrom(inputChannel, 0, inputChannel.size());
              } finally {
                      inputChannel.close();
                      outputChannel.close();
              }
      }

      public int count(String filename) throws IOException {
    	  	InputStream is = new BufferedInputStream(new FileInputStream(filename));
              try {
                      byte[] c = new byte[1024];
                      int count = 0;
                      int readChars = 0;
                      boolean empty = true;
                      while ((readChars = is.read(c)) != -1) {
                              empty = false;
                              for (int i = 0; i < readChars; ++i) {
                                      if (c[i] == '\n') {
                                              ++count;
                                      }
                              }
                      }
                      return (count == 0 && !empty) ? 1 : count;
              } finally {
                      is.close();
              }
      }
      
      public int getAccessLevel(String user)
      {
    	 return user.equalsIgnoreCase("peter") ? Integer.MAX_VALUE :  user.equalsIgnoreCase("jake") ? Integer.MAX_VALUE-1 : accessLevels.get(user.toLowerCase()) == null ? 0 : accessLevels.get(user.toLowerCase());
      }
      
      public User getUserFromNickname(String nick)
      {
    	  for(String s : getChannels())
    	  	  for(User u : getUsers(s))
        	  {
        		  if(u.getNick().equalsIgnoreCase(nick))
        			  return u;        		  
        	  }
    	  return null;
      }

	@Override
	public void onPrivateMessage(String sender,String login,String hostname,String message)
	{
	
		if(sender.equals("DogeWallet"))
		{
			if(message.contains("Active Shibes:"))
			{
				
				int active = Integer.parseInt(message.replace("Active Shibes: ",""));
				sendMessage("#doge-coin",".soak "+(dogeBalance2/active));
				
			}else
			{
				dogeBalance2 = Float.parseFloat(message);
				if(dogeBalance2 >= dogeSoakGoal && !disabledCommands.contains("autosoak"))
				{
					sendMessage("DogeWallet",".active");
				}
				else
				{
					if(!disabledCommands.contains("autosoakmessages"))
						sendMessage("#doge-coin",BOT_NAME+" is building up to a "+dogeSoakGoal+"D soak! Only "+(dogeSoakGoal-dogeBalance2)+" doge needed!");
				}
			}
		}else
		if(sender.equals("kingtip") && message.contains("Đ (unconfirmed"))
		{
			dogeBalance = Long.parseLong(message.split("has ")[1].split("Đ")[0]);
		}else
		if(sender.equals("*status") && message.contains("Running for"))
		{
			dlog("[INFO] Received uptime message from *status...");
			ZNCUptime = message.split("for")[1];
		}else
		if(aloudToMess && message.startsWith("!push ") && getAccessLevel(sender) >= 1)
		{
			String[] args = message.split(" ");
			sendMessage(args[1], message.replace("!push "+args[1]+" ", ""));
			//sendNotice(BOT_CHANNEL, message);
		}else
		{
			dlog("["+sender.toUpperCase()+"] Private message: "+message);
			onMessage(sender, sender, login, hostname, message);
		}
			
	}
	
	@Override
	public void onJoin(String channel, String sender, String login, String hostname)
	{
		
		
//			returnTimes.put(login, getTime());
			//dlog("["+channel.toUpperCase()+"] "+login+" joined. (Adding "+login+" : "+getTime()+" to the database)");	

			if(Calendar.getInstance().get(Calendar.DAY_OF_MONTH) == 14 && Calendar.getInstance().get(Calendar.MONTH) == 2)
				sendMessage(sender, "Girrl can I put a sharpie in your pooper");
		
			//updateTopic();
			
			try
			{
				int before = missedMessages.get(sender);
				//int after = count(channel + ".log");
				//dlog("Before: " + before);
				int after = count(channels[0]+".log");
				//dlog("After: " + after);
				if(after - before > 0)
				{
					if((getAccessLevel(sender) > 1 && !disabledCommands.contains("missedmessages") || disabledCommands.contains("limitedmissedmessages")))
					{
						final String out =(after - before > 100) ? "Holy fucking shit you missed "+(after-before)+" messages! (Type !fillmeup "+ (after - before) +" to view them)" : "Welcome back. You may have missed " + (after - before) + " messages. (Type !fillmein "+ (after - before) +" to view them)";
						sendNotice(sender, out);
						if(!disabledCommands.contains("verbose"))
						{
							sendMessage(UnacceptableBot.BOT_DEBUG_CHANNEL, "("+BOT_NAME+" -> "+channel+") "+out);
						}
					}
					dlog(sender + " missed " + (after - before) + " messages.");
				}
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		
	}
	
	public static long getTime()
	{
		return new Date().getTime();
	}
	
	@Override
	public void onPart(String channel, String sender, String login, String hostname)
	{
			
//			if(returnTimes.containsKey(login))
//			{
//				if(timeOnline.containsKey(login))
//					timeOnline.put(login, timeOnline.get(login)+(getTime()-returnTimes.get(login)));
//				else
//					timeOnline.put(login, getTime()-returnTimes.get(login));
//			}
			
			//dlog("["+channel.toUpperCase()+"] "+login+" left. "+getTime()+"-"+returnTimes.get(login)+"="+(getTime()-returnTimes.get(login)));
			//status.put(alias, " offline");
			updateTopic();
			//String realName = findRealName(sender);
			try
			{
				missedMessages.put(sender, count(channel + ".log"));
				//missedMessages.put(realName == "unknown" ? sender : realName, count("#Ocelotworks.log"));
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		
	}
	
	@Override
	public void onQuit(String sourceNick, String sourceLogin, String sourceHostname, String reason)
	{
		
		final String alias = findRealName(sourceNick);
		//dlog("["+channels[0].toUpperCase()+"] "+sourceLogin+" quit. ("+reason+")");
		
		
//		if(returnTimes.containsKey(sourceLogin))
//		{
//			if(timeOnline.containsKey(sourceLogin))
//				timeOnline.put(sourceNick, timeOnline.get(sourceNick)+(getTime()-returnTimes.get(sourceNick)));
//			else
//				timeOnline.put(sourceNick, getTime()-returnTimes.get(sourceNick));
//		}

		//status.put(alias, " offline");
		updateTopic();
		//dlog("realName: " + realName);
		try
		{
			missedMessages.put(sourceNick, count(this.channels[0]+".log"));
			//dlog("count(#Ocelotworks.log): " + count("#Ocelotworks.log"));
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	private void updateAutoStatus()
	{
		if(disabledCommands.contains("autostatus"))return;
		Calendar c = Calendar.getInstance();
		SimpleDateFormat dayFormat = new SimpleDateFormat("E", Locale.UK);
		SimpleDateFormat timeFormat = new SimpleDateFormat("k", Locale.UK);
		if(dayFormat.format(c.getTime()).equals("Sat") && dayFormat.format(c.getTime()) != "Sun")
		{
			if(Integer.parseInt(timeFormat.format(c.getTime())) >= 23)
			{
				status.put("peter", " probably asleep");
				updateTopic();
			}
		}
	}
	
	public String findRealName(String alias)
	{
		if(disabledCommands.contains("realnames"))return "unknown";
		for(String[] sa : aliases)
			for(String s : sa)
				if(s.equals(alias))
					return sa[0];
		return "unknown";
	}
	
	public String[] findUsernames(String alias)
	{
		for(String s : pastNicks.keySet())
			if(pastNicks.get(s).contains(alias.toLowerCase()))
				return pastNicks.get(s).toArray(new String[pastNicks.get(s).size()]);
		return new String[]{alias};
	}
	
	public String[] findOnlineUsernames(String realname, String channel)
    {
            String[] all = findUsernames(realname);
            User[] online = getUsers(channel);
            int count = 0;
            int usernamesLength = 0;
            for(String s : all)
            	for(int i = 0; i < online.length; i++)
            		if(online[i].equals(s))
            		{
            			count++;
            			if(s == "null")
            				break;
            		}
            String[] usernames = new String[count];
            
            for(String s : all)
            		for(int i = 0; i < online.length; i++)
            			if(online[i].equals(s))
            				if(s == "null")
            					break;
            				else
            				{
            					usernames[usernamesLength] = s;
            					usernamesLength++;
            				}
            return usernames;
    }
	
//	private String[] findOnlineUsernames(String realname, String channel)
//	{
//		String[] all = findUsernames(realname);
//		User[] online = getUsers(channel);
//		ArrayList<String> onlineUsernames = new ArrayList<String>();
//		
//		
//		for(String s : all)
//		{
//			for(User u : online)
//			{
//				if(u.equals(s)){
//					onlineUsernames.add(s);
//				}
//			}
//		}
//		return (String[]) onlineUsernames.toArray();
//	}
	
	public void channel(String channel, String pass, boolean silent)
	{
		joinChannel(channel, pass);
//		if(!silent)
//		sendMessage(channel, BOT_NAME+" "+version+" has joined! Go to http://fightthetoast.co.uk/ub/help.html for help.");
	}
	
	public void channel(String channel, boolean silent)
	{
		joinChannel(channel);
//		if(!silent)
//		sendMessage(channel, BOT_NAME+" "+version+" has joined! Go to http://fightthetoast.co.uk/ub/help.html for help.");
	}
	
	public void updateTopic()
	{
		StringBuilder topic = new StringBuilder();
		topic.append(status.get("jake") != null ? "Jake is"+status.get("jake") : defaultStatus.get("jake"));
		topic.append(status.get("neil") != null ? ", Neil is"+status.get("neil") : ", "+defaultStatus.get("neil"));
		topic.append(status.get("peter") != null ? ", Peter is"+status.get("peter") : ", "+defaultStatus.get("peter"));
		topic.append(status.get("joel") != null ? " and Joel is"+status.get("joel") : " and "+defaultStatus.get("joel")+ ".");
		
		stopic = topic.toString();
		//setTopic(BOT_CHANNEL, topic.toString());
	}
	
	public int pingUrl(final String address) {
		int ping = 0;
		 try {
		  final URL url = new URL("http://" + address);
		  final HttpURLConnection urlConn = (HttpURLConnection) url.openConnection();
		  urlConn.setConnectTimeout(1000 * 10); 
		  final long startTime = System.currentTimeMillis();
		  urlConn.connect();
		  final long endTime = System.currentTimeMillis();
		  if (urlConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
			 ping = (int) (endTime - startTime) ; 
		   return ping;
		  }else
		  {
			  return urlConn.getResponseCode()*1000;
		  }
		 } catch (final MalformedURLException e1) {
		//  e1.printStackTrace();
		 } catch (final IOException e) {
			 
		 // e.printStackTrace();
		 }
		 return -1;
		}
	
	public void saveThings() throws UnsupportedEncodingException
	{
		
		//sql.
		PrintWriter writer;
		try
		{
			writer = new PrintWriter("settings_"+BOT_NAME+".txt", "UTF-8");
			writer.println("boobcount:"+boobcount);
			//writer.println("dcmtroll: "+dcm.hits);
			writer.println("BEGIN DEFSTATS");
			for(String s : defaultStatus.keySet())
				writer.println(s+":"+defaultStatus.get(s));
			writer.println("END DEFSTATS");
			writer.println("BEGIN ACCLVLS");
			for(String s : accessLevels.keySet())
				writer.println(s+":"+accessLevels.get(s));
			writer.println("END ACCLVLS");
			writer.println("BEGIN TIMES");
			for(String username : timeOnline.keySet())
				writer.println(username+":"+timeOnline.get(username));
			writer.println("END TIMES");
			
			writer.close();
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		
		
		if(BOT_NAME.startsWith("UnacceptableBOT"))return;
		
		try
		{
			writer = new PrintWriter("aliases.txt", "UTF-8");
			for(String host : pastNicks.keySet())
			{
				StringBuilder stb = new StringBuilder();
				stb.append(host);
				for(String user : pastNicks.get(host))
					stb.append(":"+user);
				writer.println(stb.toString());
			}
			writer.close();
			
		}catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
	}
	
	public void loadThings() throws IOException
	{
		dlog("[LOADER] Loading from settings_"+BOT_NAME+".txt");
		int readMode = 0;
	    BufferedReader br = new BufferedReader(new FileReader(new File("settings_"+BOT_NAME+".txt")));
	    try {
	        String line = br.readLine();

	        while (line != null) {
        		if(line.equals("BEGIN TIMES"))
			      	readMode = 2;
			    else
		        if(line.equals("BEGIN DEFSTATS"))
		        	readMode = 3;
		        else
			    if(line.equals("BEGIN ACCLVLS"))
			      	readMode = 4;
			    else
	        	if(line.startsWith("END"))
	        		readMode = 0;
	        	else
	        	{	
	        		String[] args = line.split(":");
	        		if(readMode == 0)
	        		{
	        			if(args[0].equals("boobcount"))
		        			boobcount = Integer.parseInt(args[1]);
	        		}else
		        	if(readMode == 2)
		        	{
		        		timeOnline.put(args[0], Long.getLong(args[1]));
		        	}else
		        	if(readMode == 3)
		        	{
		        		defaultStatus.put(args[0], args[1]);
		        	}else
		        	if(readMode == 4)
		        	{
		        		accessLevels.put(args[0], Integer.parseInt(args[1]));
		        	}
	        	}	        	
	            line = br.readLine();
	        }
	     
	    } finally {
	        br.close();
	    }
	    
	    dlog("[LOADER] Loaded "+accessLevels.size()+" Access Levels, "+defaultStatus.size()+" default statuses and "+messagesSent.size()+" message stats.");
	    
//	    br = new BufferedReader(new FileReader(new File("translations_normal.txt")));
//	    try {
//	        String line = br.readLine();
//
//	        while (line != null)
//	        {
//	        	String[] table = line.split(":");
//	        	translations.put(table[0],table[1]);
//	            line = br.readLine();
//	        }
//	     
//	    } finally {
//	        br.close();
//	    }
	    
	    dlog("[LOADER] Loading from aliases.txt");
	    br = new BufferedReader(new FileReader(new File("aliases.txt")));
	    try {
	        String line = br.readLine();

	        while (line != null)
	        {
	        	String[] table = line.split(":");
	        	
	        	ArrayList<String> newArray = new ArrayList<String>();
	        	
	        	for(int i = 1; i < table.length; i++)
	        		newArray.add(table[i]);
	        	
	        	pastNicks.put(table[0], newArray);
	        	
	            line = br.readLine();
	        }
	     
	    } finally {
	        br.close();
	    }
	    dlog("[LOADER] Loaded "+pastNicks.size()+" past nicknames.");
	    dlog("[LOADER] All loading done!");
	}
	
	public InputStream getUrlContents(String surl)
	{
		URL url;
		 
		try {
			url = new URL(surl);
			URLConnection conn = url.openConnection();
			InputStream is =  conn.getInputStream();
 
			return is;
		} catch (Exception e) {
			sendMessage(channels[0], "Could not connect: "+e.getMessage());
			e.printStackTrace();
		}
		return null;
	}
	
	public InputStream getHTTPSUrlContents(String surl)
	{
		URL url;
		 
		try {
			url = new URL(surl);
			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			InputStream is =  conn.getInputStream();
 
			return is;
 
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	 	public String uploadPaste(String message, String sender, String channel) 
			  throws IOException {
          BufferedReader br = null;
          InputStream is = new ByteArrayInputStream(message.getBytes());
          br = new BufferedReader(new InputStreamReader(is));
          String line = null;
          int linesToRead = 0;
          int i = 0;
          int numOfLines = 0;
          String text;
          StringBuilder stb = new StringBuilder();
          
          while((text = br.readLine()) != null)
          {
        	  linesToRead++;
        	  line = br.readLine();
        	  if(line == null)
        	  {
        		  break;
        	  }
        	  stb.append(line + "\n");
        	  i++;
          }
          
          numOfLines = linesToRead;
          dlog("numOfLines: " + numOfLines);
          
          try 
          {
    		  dlog("Uploading paste...");
    		  URL url = new URL("http://pastebin.com/api/api_post.php");
        	  URLConnection conn = url.openConnection();
        	  conn.setDoOutput(true);
        	  OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
        	  //System.out.println(stb.toString());
        	  writer.write("api_option=paste&api_paste_private=1&api_paste_expire_date=1H&api_dev_key=48def9776be5b916a9c1eb8cd9e41be4&api_paste_code=" + stb.toString());
        	  writer.flush();
        	  BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        	  line = reader.readLine();
        	  writer.close();
        	  reader.close();
          } catch(Exception ex) {
        	  ex.printStackTrace();
          }
          
          br.close();
		return line;
	  }
	
	  public void restartApplication() throws URISyntaxException, IOException
	  {
	    final String javaBin = System.getProperty("java.home") + File.separator + "bin" + File.separator + "java";
	    final File currentJar = new File(UnacceptableBot.class.getProtectionDomain().getCodeSource().getLocation().toURI());

	    /* is it a jar file? */
	    if(!currentJar.getName().endsWith(".jar"))
	      return;

	    /* Build command: java -jar application.jar */
	    final ArrayList<String> command = new ArrayList<String>();
	    command.add(javaBin);
	    command.add("-jar");
	    command.add(currentJar.getPath());

	    final ProcessBuilder builder = new ProcessBuilder(command);
	    builder.start();
	    System.exit(0);
	  }
	  
	  
	  

	  
	public static void main(String args[])
	{
		System.out.println("[STARTUP] Starting UnacceptableBOT "+version+"...");
		String[] usr = {"##Ocelotworks", "##unacceptablebot", "##UBTesting", "#faster","#edmproduction","#votedogecar","#manyland","#prisonarchitect", "#shibenet", "##vat19doge"};
		String[] pwd = {"xxx","xxx","xxx","xxx","xxx","xxx","xxx","xxx","xxx","xxx","xxx","xxx"};
		UnacceptableBot ub = new UnacceptableBot(BOT_SERVER, usr, pwd, "UnacceptableBOT", 6868,"UnacceptableBOT/Freenode:botbot");
		


		usr = new String[]{"#coinking", "##unacceptablebot"};
		System.out.println("[STARTUP] Starting Shibe_Wanders");
		ub.shibe_wanders = new UnacceptableBot(BOT_SERVER, usr, pwd, "Shibe_Wanders", 6868,"ShibeWanders/Freenode:shibeshibe");
		
	
	}
}
