package com.unacceptableuse.ircbot;

import java.util.ArrayList;

import org.pircbotx.Configuration;
import org.pircbotx.PircBotX;
import org.pircbotx.UtilSSLSocketFactory;
import org.pircbotx.cap.SASLCapHandler;
import org.pircbotx.cap.TLSCapHandler;
import org.pircbotx.hooks.Event;
import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.MessageEvent;
import org.pircbotx.hooks.types.GenericMessageEvent;

import com.unacceptableuse.ircbot.command.Command;

public class UnacceptableBotX extends ListenerAdapter
{

	public ArrayList<Command> commands = new ArrayList<Command>();
	public ArrayList<String> disabledCommands = new ArrayList<String>();
	
//	public static void main(String[] args)
//	{
//		startBot("UnacceptableBOT","UnaccB","irc.freenode.net",6667, "##Ocelotworks");
//	}
	
	@SuppressWarnings("unchecked")
	public static void startBot(String name, String login, String server,Integer port, String channel)
	{
		Configuration configuration = new Configuration.Builder()
				.setName(name)
				.setLogin(login)
				.setAutoNickChange(true)
				.setCapEnabled(true)
				.addCapHandler(new TLSCapHandler(new UtilSSLSocketFactory().trustAllCertificates(), true))
				.addCapHandler(new SASLCapHandler("UnacceptableBOT", "11114444"))
				.setServerHostname(server)
				.setServerPort(port)
				.addAutoJoinChannel(channel)
				.buildConfiguration();
		
		
		try
		{
			PircBotX bot = new PircBotX(configuration);
			bot.getListener
			bot.startBot();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
				
	}


	 @Override
     public void onGenericMessage(final GenericMessageEvent event) throws Exception
     {

		 event.respond("test");
//		 for(Command c : commands)
//		 {
//			 for(String s : c.getAliases())
//			 {
//				 if(event.getMessage().startsWith("!"+s+" ") && !disabledCommands.contains(s))
//					 c.excecuteCommand(event.getChannel().getName(), event.getUser().getNick(), event.getMessage());
//			 }
//		 }
	 }
	
	
	@Override
	public void onEvent(Event arg0) throws Exception
	{
	}
}
