package com.unacceptableuse.ircbot;

public class ThreadPollCommands implements Runnable
{

    private UnacceptableBot ub;

    public ThreadPollCommands(UnacceptableBot ub) {
        this.ub = ub;
    }
	@Override
	public void run()
	{
		while(true)
		{
			ub.pollCommands();
			if(ub.BOT_NAME.contains("Shibe_Wanders"))
				ub.pollBlocks();
			try
			{
				Thread.sleep(1000);
			} catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			
		}
	}

}
