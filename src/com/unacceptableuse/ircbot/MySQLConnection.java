package com.unacceptableuse.ircbot;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;



public class MySQLConnection
{
 private static final String dbClassName = "com.mysql.jdbc.Driver";
	 
	 private static final String CONNECTION =
			 "jdbc:mysql://127.0.0.1/";
	 
	 
	 protected Connection c;

	
	public void connect() throws SQLException, ClassNotFoundException
	{
		 Class.forName(dbClassName);

	    Properties p = new Properties();
	    p.put("user","root");
	    p.put("password","11114444");
	
	    c = DriverManager.getConnection(CONNECTION,p);

	}
	
	
	public boolean createChannelTable(String channel) 
	{
		try
		{
			return excecute("CREATE TABLE IF NOT EXISTS 'Channels'.'"+channel+"'('Time' timestamp,'Username' text,'Message' text);");
		} catch (SQLException e)
		{
			e.printStackTrace();
			System.err.println("Error occurred making channel table");
			return false;
		}

	}
	
	
	
	public String getSetting(String setting)
	{
		try
		{
			ResultSet rs = query("SELECT * FROM  Settings.Global_settings WHERE  `Setting` =  '"+setting+"' LIMIT 1");
			return rs.next() ? rs.getString(2) : null;
		} catch (SQLException e)
		{
			e.printStackTrace();
			return "Error "+e.getErrorCode()+" "+e.getLocalizedMessage();
		}
	}
	
	public boolean setSetting(String setting, String value)
	{
		try
		{
			return excecute("INSERT INTO `Settings`.`Global_settings` (`Setting`, `Value`) VALUES ('"+setting+"', '"+value+"');");
		} catch (SQLException e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	
	public void logMessage(String channel, String sender, String message)
	{
		try
		{
			excecute("INSERT INTO `Channels`.`"+channel+"` (`Time`, `Username`, `Message`) VALUES (CURRENT_TIMESTAMP, '+sender+', '+message+');");
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public ResultSet query(String sql) throws SQLException
	{
		return c.prepareStatement(sql).executeQuery();
	}
	
	public boolean excecute(String sql) throws SQLException
	{
		return c.prepareStatement(sql).execute();
	}
	
	public void disconnect() throws SQLException
	{
		 c.close();
	}
}
