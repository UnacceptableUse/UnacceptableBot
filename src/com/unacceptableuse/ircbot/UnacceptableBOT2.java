package com.unacceptableuse.ircbot;

import java.sql.SQLException;

import org.jibble.pircbot.PircBot;

public class UnacceptableBOT2 extends PircBot
{

	public static String version = "V2 BUILD 173";
	private final CommandHandler ch = new CommandHandler(this);
	protected MySQLConnection sql;
	
	
	public UnacceptableBOT2()
	{
		init();
	}
	
	public static void dlog(String message)
	{
		System.out.println(message);
	}
	
	public static void elog(String message)
	{
		System.err.println("[ERROR] "+message);
	}
	
	private void establishMySQLConnection()
	{
		dlog("[MYSQL] Connecting to MySQL Database...");
		sql = new MySQLConnection();
		try
		{
			sql.connect();
		} catch (ClassNotFoundException e)
		{
			elog("[MYSQL] MySQL Driver "+e.getMessage()+" not found!");
			e.printStackTrace();
		} catch (SQLException e)
		{
			elog("[MYSQL] Unable to connect: MYSQL #"+e.getErrorCode());
			e.printStackTrace();
		}
	}
	
	protected void init()
	{
		dlog("[INIT] Initializing...");
		establishMySQLConnection();
		ch.registerCommands();
		
	}
	
	
	
	@Override
	public void onMessage(String channel, String sender, String login, String hostname, String message)
	{
		ch.handleCommand(sender, channel, message);
	}
}
